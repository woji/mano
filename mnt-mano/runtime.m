runtime ; awport.pro, 2022-01-15, please read LICENSE before use
  ; base functs for automated, safe routine load
  ; coz if 2 routine files has same name (in different folders)
  ; gtm uses lastly linked only = replaces original with the new one !!!
  if ('$data(^mlangRuntime)) do defaults
  Q

; there are 2 types of routine file:
; module - contains just a one (few) functions (library component)
;  module filename should be like (directory wide) distinguished name (DN)
;  and it follows its codebase path
;  module filename should be camelCase capitalized (readability)
;  eg. json/parse file has label jsonParse.m
;
; library - all in one (can autogenerate), larger set of functions, eg. whole json processor
;  library filename should be like relative distiguishedname RDN
;  mostly, it must be directory wide unique word
; exception is when interface mimics in m-lang
;
; must avoid all m-name prohibited chars in codebase paths
; so just %?[A-Za-z0-9] are valid in path+".m" as file extension
; "%" needs to be replaced with "_" in filesystem
; also, m-lang treats just first 31 chars from label (older systems just 9)
;



;alias for import
i(mlangNs) do import(.mlangNs) Q

import(mlangNs) ;import module/routine
  ; example #1: do import^runtime("http/req")
  ; translates into ZLINK "[dockerRoutinePath]/http/req.m"
  ; example #2: do import^runtime("http")
  ; translates into ZLINK "[dockerRoutinePath]/http/http.m"
  ; then in program call is parse^reqHttp()
  ;
  ; dockerRoutinePath is always /data/r/, see getRoutinePath()

  ;first time run, make it current process global
  if ('$data(^mlangRuntime)) do defaults

  ;new %mfile,%mfile1,%mfile2,%mfile3,%mfile4,mlangLabL,mlangLabS
  N %mfile,mlangLibNS
  ;with routines path...
  ;S %mfile=$ZROUTINES_"/"_mlangNs

  ;if path begins with "/" dont add mano to path
  if ($piece(mlangNs,"/",1)="") do
  . S %mfile=$$getRoutinePath()_$extract(mlangNs,2,999)
  else  S %mfile=$$getRoutinePath()_"mano/"_mlangNs

  ;S %mfile=$$getRoutinePath()_mlangNs
  D trace("D>"_%mfile)

  S %mfile(1)=$zsearch(%mfile)
  S %mfile(2)=$zsearch(%mfile_".m")
  S %mfile(3)=$zsearch(%mfile_"/"_$$getMlangLibName(mlangNs)_".m")
  S %mfile(4)=$zsearch(%mfile_"/"_$$getMlangLibNameS(mlangNs)_".m")
  ;zwrite %mfile
  if (%mfile(1)=""&%mfile(2)=""&%mfile(3)=""&%mfile(4)="") open %mfile1 ;throws file not found ex

  if (%mfile(4)'="") S %mfile=%mfile(4) ;found library
  else  if (%mfile(3)'="") S %mfile=%mfile(3) ;found library
  else  if (%mfile(1)'=""&%mfile(2)="") S %mfile=%mfile(1) ;found routine
  else  S %mfile=%mfile(2)

  if (%mfile(4)'=""!%mfile(2)'="") S mlangLibNS=$$getMlangLibNameS(mlangNs)
  else  S mlangLibNS=$$getMlangLibName(mlangNs)

  ;write %mfile,!
  ;check for .m extension at end
  ;new %ex S %ex=0
  ;set:($extract(mlngNs,$length(mlngNs)-1,$length(mlngNs))=".m") %ex=1
  ; has .m at end (by mistake)?, just remove it
  ;if '($find(mlngNs,".m")=$length(mlngNs)) S mlngNs=$extract(mlngNs,1,$length(mlngNs)-2)

  ; do link module from path
  zlink %mfile
  ;uncomment next line to see imports in trace
  ;D trace("D>"_%mfile)
  ;
  ; call initialize routine
  ; only when it is library ?? or always ??
  ;if (%mfile=%mfile(3)!%mfile=%mfile(4)) do @("init^"_mlangLabel)
  ;W mlangLibNS,!
  D @("init^"_mlangLibNS)
  Q



;alias for run
r(ns,cmd,arg1,arg2,arg3,arg4,arg5)
  D run(.ns,.cmd,.arg1,.arg2,.arg3,.arg4,.arg5)
  Q



;runs given rutine by namespace, cmd and arguments
;in new job with also opening console output
rj(ns,cmd,arg1,arg2,arg3,arg4,arg5)
  ;new job, might not have opened term for trace
  O:($data(^mlangRuntime("config","trace","outTerm"))) ^mlangRuntime("config","trace","outTerm")
  D run(.ns,.cmd,.arg1,.arg2,.arg3,.arg4,.arg5)
  Q

;run in same process (no need to open stdio for trace)
run(ns,cmd,arg1,arg2,arg3,arg4,arg5)
  if ('$data(^mlangRuntime)) do defaults
  N i,args,arg S args="",sep=""
  for i=1:1:5 Q:('$data(@("arg"_i)))  S arg=@("arg"_i) do
  . S:(args?.N) arg=""""_arg_"""" ;not numeric
  . S args=args_sep_arg,sep=","
  ;if ($data(arg1)) S args=args_arg1
  ;do trace(ns)
  D trace("D>runtime.RunJob:"_cmd_"("_args_")")
  ;todo: when compiled dont load ns as well!!!
  D:($get(ns,"")'="") i^runtime(ns)
  X "D "_cmd_"("_args_")"
  Q



; private get m-lang assembly/library filename from given path
; eg. /xml/dom/tokenizer -> xmlDomTokenizer
; this is by convention, library filename (+ .m extension)
getMlangLibName:(mlangNs)
  N %l S %l=$length(mlangNs,"/")
  N retV S retV=$piece(mlangNs,"/",1)
  if (%l>1) for i=2:1:%l S retV=retV_$zconvert($piece(mlangNs,"/",i),"T")
  ;todo: remove non alfanum!!
  ;W retV,!
  Q retV



;get short name
;when libname is global unique RDN no need for long name
getMlangLibNameS:(mlangNs)
  N %l S %l=$length(mlangNs,"/")
  N retV S retV=$piece(mlangNs,"/",%l)
  ;W retV,!
  Q retV


getRoutinePath:()
  Q "/mano/" ;this is fixed for container version



; random UUID generator produces out as: 2545ec68-9774-46ae-a16b-3e67e876d8d4
;https://groups.google.com/g/hardhats/c/GDUBcZF8Zb0/m/Wm_BXsA0JocJ
getId() ;
  ;todo more platforms
  D trace^runtime("getId")
  S uuid="/proc/sys/kernel/random/uuid"
  open uuid:readonly use uuid read % use $principal close uuid
  Q %

UUID() ; GENERATE A RANDOM UUID (Version 4), Wally full m version (not recomended, bad entropy)
  N R,I,J,N
  S N="",R="" F  S N=N_$R(100000) Q:$L(N)>64
  F I=1:2:64 S R=R_$E("0123456789abcdef",($E(N,I,I+1)#16+1))
  Q $E(R,1,8)_"-"_$E(R,9,12)_"-4"_$E(R,14,16)_"-"_$E("89ab",$E(N,17)#4+1)_$E(R,18,20)_"-"_$E(R,21,32)



;trace w/ console output
traceDev:(dev,MSG)
  ;pozor mozna bude potreba $ZIO
  ;W $IO,!,$ZIO,! nevidim rozdil...
  N %devN S %devN=$IO use dev
  N % S %=$H W $ZDATE(%,"YYYY-MM-DD")_"T"_$ZDATE(%,"24:60:SS")_" "_MSG,!
  ;if (MSG="w3job.sleep()") ZSH "D"
  use %devN
  Q



;trace to console/terminal or to global
trace(MSG)
  Q:($get(^mlangRuntime("config","trace"),"")="") ;trace disabled
  S MSG=$get(MSG,"");if no msg supplied, use empty string
  Q:(^mlangRuntime("config","trace")="I"&(MSG["D>")) ;debug disabled
  S:(^mlangRuntime("config","trace")="D"&(MSG["D>")) MSG=$extract(MSG,3,$L(MSG))
  if ($data(^mlangRuntime("config","trace","outTerm"))) do traceDev(^mlangRuntime("config","trace","outTerm"),.MSG) Q
  N % S %=$H
  ;W i,!
  S ^mlangRuntime("run","trace",$incr(i))=$ZDATE(%,"YYYY-MM-DD")_"T"_$ZDATE(%,"24:60:SS")_" "_MSG
  Q



; setup tracing
; trcLevel values ("I","D",""), D reacts on symbol before MSG "D>" - debug, "I" -informational, "" -no output
; trOutTerm set trace output, if no value specified, trace stores in global ^mlangRuntime("run","trace")
setTrace(trcLevel,trOutTerm) ;bool value
  if ('$data(^mlangRuntime)) do defaults
  K ^mlangRuntime("config","trace")
  S ^mlangRuntime("config","trace")=trcLevel
  K:(trcLevel'="") ^mlangRuntime("run","trace")
  S:($data(trOutTerm)) ^mlangRuntime("config","trace","outTerm")=trOutTerm
  Q



;ballast, remove in future
vTrace()
  ZWR:($data(^mlangRuntime("run","trace"))) ^mlangRuntime
  Q



;return engine name
engine()
  Q $select($piece($SY,",")=47:"GT.M",$piece($SY,",")=50:"MV1",1:"CACHE")



; load runtime defaults
defaults:
  K ^mlangRuntime;,mlangRuntime N mlangRuntime
  S ^mlangRuntime("ver")="0.0.1",^mlangRuntime("config","trace")=1
  ;copy global into mem var
  ;merge mlangRuntime=^mlangRuntime
  Q



errReport()
  ;s $ETRAP="Q"
  D trace^runtime("D>err")
  D trace^runtime($ZSTATUS)
  D trace^runtime($ZERROR)
  Q
