init ; awport.pro, 2022-01-16, please read LICENSE before use
  do import^runtime("json/jsonParse")
  do import^runtime("json/jsonStringify")
  Q



stringify(glvn) q $$stringify^jsonStringify(.glvn)
;stringify(glv) q "$data(glv)"
parse(glvn) q $$parse^jsonStringify(.glvn)


test
  do init
  do test^jsonStringify
  do test^jsonParse
  Q
