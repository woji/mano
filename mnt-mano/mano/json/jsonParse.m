init ;awport.pro, 2022-01-18, please read LICENSE before use
  do import^runtime("json/jsonErr")
  ;do import^runtime("runtime")
  q
;
;
; Portions of this code are public domain, but it was extensively modified
; Copyright 2016 Accenture Federal Services
; Copyright 2013-2019 Sam Habiel
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;    http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.


; parses json string data into "fromJson" memory variable
; todo: errors not handled !!
parse(jsonStr)
  D trace^runtime("D>parse()")
  K fromJson set fromJson="" ;make output variable (global scope), with null
  N retVal set retVal=$name(fromJson) ;copy its name
  D parseAdd(.retVal,.jsonStr)
  Q retVal ;just because q "retval" is impossible


;parseAdd parsed nodes into specified variable eg. parseAdd("^adam(3)",json)
parseAdd(src,VVJSON)
  D trace^runtime("D>parseAdd()")
  N VVROOT,VVERR,SQ,SQ1,SQ2 ;SQ = string qualifier
  set VVERR="",VVROOT=src,SQ1="""",SQ2="'",SQ=""
 ;
 ; Examples: do DECODE^%webjson("MYJSON","LOCALVAR","LOCALERR")
 ;           do DECODE^%webjson("^MYJSON(1)","^GLO(99)","^TMP($J)")
 ;
 ; VVJSON: string/array containing serialized JSON object
 ; VVROOT: closed array reference for M representation of object
 ;  VVERR: contains error messages, defaults to ^TMP("%webjsonerr",$J)
 ;
 ;   VVIDX: points to next character in JSON string to process
 ; VVSTACK: manages stack of subscripts
 ;  VVPROP: true if next string is property name, otherwise treat as value
 ;
 ; V4W/DLW - Changed VVMAX from 4000 to 100, same as in the encoder
 ; With the change to VVMAX, the following Unit Tests required changes:
 ; SPLITA^%webjsonDecodeTest, SPLITB^%webjsonDecodeTest, LONG^%webjsonDecodeTest, MAXNUM^%webjsonDecodeTest
 N VVMAX set VVMAX=100 ; limit document lines to 100 characters
 set VVERR=$get(VVERR,"^TMP(""%webjsonerr"",$J)")
 ; If a simple string is passed in, move it to an temp array (VVINPUT)
 ; so that the processing is consistently on an array.
 if $data(@VVJSON)=1 new VVINPUT set VVINPUT(1)=@VVJSON,VVJSON="VVINPUT"

 set VVROOT=$name(@VVROOT@("Z"))
 ; make textual open array ref, like "var("
 set VVROOT=$extract(VVROOT,1,$length(VVROOT)-4) ; make open array ref

 N VVLINE,VVIDX,VVSTACK,VVPROP,VVTYPE,VVERRORS
 set VVLINE=$order(@VVJSON@("")),VVIDX=1,VVSTACK=0,VVPROP=0,VVERRORS=0
 for  set VVTYPE=$$NXTKN() quit:VVTYPE=""  do  if VVERRORS Q
 . if VVTYPE="{" set VVSTACK=VVSTACK+1,VVSTACK(VVSTACK)="",VVPROP=1 D:VVSTACK>64 ERRX("STL{") Q
 . if VVTYPE="}" do  quit
 . . if VVSTACK'>0 do ERRX("SUF}") quit  ; Extra Brace. Nothing to pop.
 . . if VVSTACK(VVSTACK)?1n.n,VVSTACK(VVSTACK) do ERRX("OBM") quit  ; Numeric and true only
 . . set VVSTACK=VVSTACK-1
 . if VVTYPE="[" set VVSTACK=VVSTACK+1,VVSTACK(VVSTACK)=1 D:VVSTACK>64 ERRX("STL[") Q
 . if VVTYPE="]" D:'VVSTACK(VVSTACK) ERRX("ARM") set VVSTACK=VVSTACK-1 D:VVSTACK<0 ERRX("SUF]") Q
 . ;
 . ; At this point, we should be in a brace or a bracket (indicated by VVSTACK>0)
 . ; If not we have an error condition
 . if VVSTACK'>0 do ERRX("TRL",VVTYPE) Q
 . if VVTYPE="," do  quit
 . . if +VVSTACK(VVSTACK)=VVSTACK(VVSTACK),VVSTACK(VVSTACK) set VVSTACK(VVSTACK)=VVSTACK(VVSTACK)+1  ; VEN/SMH - next in array
 . . else  set VVPROP=1                                   ; or next property name
 . if VVTYPE=":" set VVPROP=0 D:'$length($get(VVSTACK(VVSTACK))) ERRX("MPN") Q
 . ;awport.pro update
 . if ((VVTYPE=SQ1)!(VVTYPE=SQ2)) do  quit
 . . do setSQ(VVTYPE) ;always ensure is set string qualifier
 . . if VVPROP set VVSTACK(VVSTACK)=$$UES($$NAMPARS(),1) if 1
 . . else  do ADDSTR
 . set VVTYPE=$TR(VVTYPE,"TFN","tfn")
 . if VVTYPE="t" do SETBOOL("t") Q
 . if VVTYPE="f" do SETBOOL("f") Q
 . if VVTYPE="n" do SETBOOL("n") Q
 . if "0123456789+-.eE"[VVTYPE do SETNUM(VVTYPE) quit  ;S @$$CURNODE()=$$NUMPARS(VVTYPE) Q
 . do ERRX("TKN",VVTYPE)
 if VVSTACK'=0 do ERRX("SCT",VVSTACK)
 Q ;$e(VVROOT,1,$l(VVROOT)-1) ;as funct, drop open bracket from its end



;
setSQ:(dataSQ)
  ;D trace^runtime("D>setSQ "_dataSQ)
  if SQ="" set SQ=dataSQ ;only for first time
  Q




;
NXTKN() ; Move the pointers to the beginning of the next token
 new VVDONE,VVEOF,VVTOKEN
 set VVDONE=0,VVEOF=0 for  do  quit:VVDONE!VVEOF  ; eat spaces & new lines until next visible char
 . if VVIDX>$length(@VVJSON@(VVLINE)) set VVLINE=$order(@VVJSON@(VVLINE)),VVIDX=1 if 'VVLINE set VVEOF=1 Q
 . if $ascii(@VVJSON@(VVLINE),VVIDX)>32 set VVDONE=1 Q
 . set VVIDX=VVIDX+1
 quit:VVEOF ""  ; we're at the end of input
 set VVTOKEN=$extract(@VVJSON@(VVLINE),VVIDX),VVIDX=VVIDX+1
 quit VVTOKEN



;
ADDSTR ; Add string value to current node, escaping text along the way
 ; Expects VVLINE,VVIDX to reference that starting point of the index
 ; TODO: add a mechanism to specify names that should not be escaped
 ;       just store as ":")= and ":",n)=
 ;
 ; Happy path -- we find the end quote in the same line
 new VVEND,VVX
 set VVEND=$find(@VVJSON@(VVLINE),SQ,VVIDX)
 if VVEND,($extract(@VVJSON@(VVLINE),VVEND-2)'="\") do SETSTR  quit  ;normal
 if VVEND,$$ISCLOSEQ(VVLINE) do SETSTR quit  ;close quote preceded by escaped \
 ;
 ; Less happy path -- first quote wasn't close quote
 new VVDONE,VVTLINE
 set VVDONE=0,VVTLINE=VVLINE ; VVTLINE for temporary increment of VVLINE
 for  do  quit:VVDONE  quit:VVERRORS
 . ;if no quote on current line advance line, scan again
 . if 'VVEND set VVTLINE=VVTLINE+1,VVEND=1 if '$data(@VVJSON@(VVTLINE)) do ERRX("EIQ") Q
 . set VVEND=$find(@VVJSON@(VVTLINE),SQ,VVEND)
 . quit:'VVEND  ; continue on to next line if no quote found on this one
 . if (VVEND>2),($extract(@VVJSON@(VVTLINE),VVEND-2)'="\") set VVDONE=1 quit  ; found quote position
 . set VVDONE=$$ISCLOSEQ(VVTLINE) ; see if this is an escaped quote or closing quote
 quit:VVERRORS
 ; unescape from VVIDX to VVEND, using \-extension nodes as necessary
 do UESEXT
 ; now we need to move VVLINE and VVIDX to next parsing point
 set VVLINE=VVTLINE,VVIDX=VVEND
 Q



;
SETSTR ; Set simple string value from within same line
 ; expects VVJSON, VVLINE, VVINX, VVEND
 new VVX
 set VVX=$extract(@VVJSON@(VVLINE),VVIDX,VVEND-2),VVIDX=VVEND
 set @$$CURNODE()=$$UES(VVX)
 ; "\s" node indicates value is really a string in case value
 ;      collates as numeric or equals boolean keywords
 if VVX']]$char(1) set @$$CURNODE()@("\s")=""
 if VVX="true"!(VVX="false")!(VVX="null") set @$$CURNODE()@("\s")=""
 if VVIDX>$length(@VVJSON@(VVLINE)) set VVLINE=VVLINE+1,VVIDX=1
 Q



;
UESEXT ; unescape from VVLINE,VVIDX to VVTLINE,VVEND & extend (\) if necessary
 ; expects VVLINE,VVIDX,VVTLINE,VVEND
 new VVI,VVY,VVSTART,VVSTOP,VVDONE,VVBUF,VVNODE,VVMORE,VVTO
 set VVNODE=$$CURNODE(),VVBUF="",VVMORE=0,VVSTOP=VVEND-2
 set VVI=VVIDX,VVY=VVLINE,VVDONE=0
 for  do  quit:VVDONE  quit:VVERRORS
 . set VVSTART=VVI,VVI=$find(@VVJSON@(VVY),"\",VVI)
 . ; if we are on the last line, don't extract past VVSTOP
 . if (VVY=VVTLINE) set VVTO=$select('VVI:VVSTOP,VVI>VVSTOP:VVSTOP,1:VVI-2) if 1
 . else  set VVTO=$select('VVI:99999,1:VVI-2)
 . do ADDBUF($extract(@VVJSON@(VVY),VVSTART,VVTO))
 . if (VVY'<VVTLINE),(('VVI)!(VVI>VVSTOP)) set VVDONE=1 quit  ; now past close quote
 . if 'VVI set VVY=VVY+1,VVI=1 quit  ; nothing escaped, go to next line
 . if VVI>$length(@VVJSON@(VVY)) set VVY=VVY+1,VVI=1 if '$data(@VVJSON@(VVY)) do ERRX("EIU")
 . new VVTGT set VVTGT=$extract(@VVJSON@(VVY),VVI)
 . if VVTGT="u" do  if 1
 . . new VVTGTC set VVTGTC=$extract(@VVJSON@(VVY),VVI+1,VVI+4),VVI=VVI+4
 . . if $length(VVTGTC)<4 set VVY=VVY+1,VVI=4-$length(VVTGTC),VVTGTC=VVTGTC_$extract(@VVJSON@(VVY),1,VVI)
 . . do ADDBUF($char($$DEC^%webutils(VVTGTC,16)))
 . else  do ADDBUF($$REALCHAR(VVTGT))
 . set VVI=VVI+1
 . if (VVY'<VVTLINE),(VVI>VVSTOP) set VVDONE=1 ; VVI incremented past stop
 quit:VVERRORS
 do SAVEBUF
 Q



;
ADDBUF(VVX) ; add buffer of characters to destination
 ; expects VVBUF,VVMAX,VVNODE,VVMORE to be defined
 ; used directly by ADDSTR
 if $length(VVX)+$length(VVBUF)>VVMAX do SAVEBUF
 set VVBUF=VVBUF_VVX
 Q



;
SAVEBUF ; write out buffer to destination
 ; expects VVBUF,VVMAX,VVNODE,VVMORE to be defined
 ; used directly by ADDSTR,ADDBUF
 if VVMORE set @VVNODE@("\",VVMORE)=VVBUF
 if 'VVMORE set @VVNODE=VVBUF if $length(VVBUF)<19,+$extract(VVBUF,1,18) set @VVNODE@("\s")=""
 set VVMORE=VVMORE+1,VVBUF=""
 Q



;
ISCLOSEQ(VVBLINE) ; return true if this is a closing, rather than escaped, quote
 ; expects
 ;   VVJSON: lines of the JSON encoded string
 ;    VVIDX: points to 1st character of the segment
 ;   VVLINE: points to the line in which the segment starts
 ;    VVEND: points to 1st character after the " (may be past the end of the line)
 ; used directly by ADDSTR
 new VVBS,VVBIDX,VVBDONE
 set VVBS=0,VVBIDX=VVEND-2,VVBDONE=0 ; VVBIDX starts at 1st character before quote
 ; count the backslashes preceding the quote (odd number means the quote was escaped)
 for  do  quit:VVBDONE!VVERRORS
 . if VVBIDX<1 do  quit  ; when VVBIDX<1 go back a line
 . . set VVBLINE=VVBLINE-1 if VVBLINE<VVLINE do ERRX("RSB") Q
 . . set VVBIDX=$length(@VVJSON@(VVBLINE))
 . if $extract(@VVJSON@(VVBLINE),VVBIDX)'="\" set VVBDONE=1 Q
 . set VVBS=VVBS+1,VVBIDX=VVBIDX-1
 quit VVBS#2=0  ; VVBS is even if this is a close quote



;
NAMPARS() ; Return parsed name, advancing index past the close quote
 new VVEND,VVDONE,VVNAME,VVSTART
 set VVDONE=0,VVNAME="",VVSTART=VVIDX
 for  do  quit:VVDONE  quit:VVERRORS
 . set VVEND=$find(@VVJSON@(VVLINE),SQ,VVIDX)
 . if $extract(@VVJSON@(VVLINE),VVEND-2)="\" set VVIDX=VVEND Q
 . if VVEND set VVNAME=VVNAME_$extract(@VVJSON@(VVLINE),VVSTART,VVEND-2),VVIDX=VVEND,VVDONE=1
 . if 'VVEND set VVNAME=VVNAME_$extract(@VVJSON@(VVLINE),VVSTART,$length(@VVJSON@(VVLINE)))
 . if 'VVEND!(VVEND>$length(@VVJSON@(VVLINE))) set VVLINE=VVLINE+1,(VVIDX,VVSTART)=1 if '$data(@VVJSON@(VVLINE)) do ERRX("ORN")
 ; prepend quote if label collates as numeric -- assumes no quotes in label
 if VVNAME']]$char(1) set VVNAME=SQ1_SQ1_VVNAME
 quit VVNAME



;
SETNUM(VVDIGIT) ; Set numeric along with any necessary modifier
 new VVX
 set VVX=$$NUMPARS(VVDIGIT)
 set @$$CURNODE()=$select(VVX["e":+$TR(VVX,"e","E"),1:+VVX)
 ; if numeric is exponent, "0.nnn" or "-0.nnn" store original string
 if +VVX'=VVX set @$$CURNODE()@("\n")=VVX
 Q



;
NUMPARS(VVDIGIT) ; Return parsed number, advancing index past end of number
 ; VVIDX intially references the second digit
 new VVDONE,VVNUM
 set VVDONE=0,VVNUM=VVDIGIT
 for  do  quit:VVDONE  quit:VVERRORS
 . if '("0123456789+-.eE"[$extract(@VVJSON@(VVLINE),VVIDX)) set VVDONE=1 Q
 . set VVNUM=VVNUM_$extract(@VVJSON@(VVLINE),VVIDX)
 . set VVIDX=VVIDX+1 if VVIDX>$length(@VVJSON@(VVLINE)) set VVLINE=VVLINE+1,VVIDX=1 if '$data(@VVJSON@(VVLINE)) do ERRX("OR#")
 quit VVNUM



;
SETBOOL(VVLTR) ; Parse and set boolean value, advancing index past end of value
 new VVDONE,VVBOOL,VVX
 set VVDONE=0,VVBOOL=VVLTR
 for  do  quit:VVDONE  quit:VVERRORS
 . set VVX=$TR($extract(@VVJSON@(VVLINE),VVIDX),"TRUEFALSN","truefalsn")
 . if '("truefalsn"[VVX) set VVDONE=1 Q
 . set VVBOOL=VVBOOL_VVX
 . set VVIDX=VVIDX+1 if VVIDX>$length(@VVJSON@(VVLINE)) set VVLINE=VVLINE+1,VVIDX=1 if '$data(@VVJSON@(VVLINE)) do ERRX("ORB")
 if VVLTR="t",(VVBOOL'="true") do ERRX("EXT",VVTYPE)
 if VVLTR="f",(VVBOOL'="false") do ERRX("EXF",VVTYPE)
 if VVLTR="n",(VVBOOL'="null") do ERRX("EXN",VVTYPE)
 set @$$CURNODE()=VVBOOL
 Q



;
OSETBOOL(VVX) ; set a value and increment VVIDX
 set @$$CURNODE()=VVX
 set VVIDX=VVIDX+$length(VVX)-1
 new VVDIFF set VVDIFF=VVIDX-$length(@VVJSON@(VVLINE))  ; in case VVIDX moves to next line
 if VVDIFF>0 set VVLINE=VVLINE+1,VVIDX=VVDIFF if '$data(@VVJSON@(VVLINE)) do ERRX("ORB")
 Q



;
CURNODE() ; Return a global/local variable name based on VVSTACK
 ; Expects VVSTACK to be defined already
 new VVI,VVSUBS
 set VVSUBS=""
 for VVI=1:1:VVSTACK S:VVI>1 VVSUBS=VVSUBS_"," D
 . ; check numeric with pattern match instead of =+var due to GT.M interperting
 . ; scientific notation as a number instead of a string
 . if VVSTACK(VVI)?1N.N set VVSUBS=VVSUBS_VVSTACK(VVI) ; VEN/SMH Fix psudo array bug.
 . else  set VVSUBS=VVSUBS_SQ1_VVSTACK(VVI)_SQ1
 ;w VVROOT_VVSUBS_")"
 quit VVROOT_VVSUBS_")"



;
UES(X,KEY) ; Unescape JSON string
 ; copy segments from START to POS-2 (right before \)
 ; translate target character (which is at $F position)
 new POS,Y,START
 set POS=0,Y=""
 for  set START=POS+1 do  quit:START>$length(X)
 . set POS=$find(X,"\",START) ; find next position
 . if 'POS set Y=Y_$extract(X,START,$length(X)),POS=$length(X) Q
 . ; otherwise handle escaped char
 . new TGT
 . set TGT=$extract(X,POS),Y=Y_$extract(X,START,POS-2)
 . ; decode \uxxxx hex chars
 . if TGT="u" set Y=Y_$char($$DEC^%webutils($extract(X,POS+1,POS+4),16)),POS=POS+4 Q
 . set Y=Y_$$REALCHAR(TGT,$get(KEY))
 quit Y



;
REALCHAR(C,KEY) ; Return actual character from escaped
 if C=SQ&'$get(KEY) quit SQ_SQ ; Used in the value part and doesn't need double quotes
 if C=SQ&$get(KEY) quit  ; Used as part of a set statement and needs double quotes
 if C="/" quit "/"
 if C="\" quit "\"
 if C="b" quit $char(8)
 if C="f" quit $char(12)
 if C="n" quit $char(10)
 if C="r" quit $char(13)
 if C="t" quit $char(9)
 if C="u" ;case covered above in $$DEC^%webutils calls
 ;otherwise
 if $length($get(VVERR)) do ERRX("ESC",C)
 Q C




;need to redone fully :(
ERRX(ID,VAL) ; Set the appropriate error message
 do ERRX^jsonErr(ID,$get(VAL))
 ;w ID_VAL,!
 Q


test
  do init
  N testIn,testOut
  set testIn="444{'data':'Click ""Here','size':36,'style':'bold','name':'text1','hOffset':250,'vOffset':100,'alignment':'center','onMouseUp':'sun1.opacity = (sun1.opacity / 100) * 90;'}"
  zwrite @($$parse("testIn"))

  set testIn="{""data"":""Click 'Here"",""size"":36,""style"":""bold"",""name"":""text1"",""hOffset"":250,""vOffset"":100,""alignment"":""center"",""onMouseUp"":""sun1.opacity = (sun1.opacity / 100) * 90;""}"
  zwrite @($$parse("testIn"))
  ;set testOut=$$parse("testIn")
  ;zwrite @(testOut)
  q
