init() ; awport.pro, 2022-01-15, please read LICENSE before use
;JAVASCRIPT OBJECT NOTATION BUILDER;;V1.00;;NOV2008;;TSG;;
  ;
	; set OF FUNCTIONS TO BUILD JSON STRINGS
	;
	; JSON (JavaScript Object Notation) is a lightweight data-interchange format. JSON is built on two structures:
	; - An object is an unordered set of name/value pairs.
	; - An array is an ordered collection of values.
	;
	; Usage:
	;
	; Code obtained from the link:
	; http://www.cachewiki.org/index.php/JavaScript_ObjectScript_Notation_(JSON)_Builder
	; Original Author Tony Toucan toucan@spuddy.mew.co.uk
  ; adopted by AWPORT.PRO
	; see test at bottom
	; removed json spacing !!! pure json only
	;
	;	numeric indexes are serialized as array, text ones as objects
	;
	; limitation: don't mix value and subscripts on node, like this:
	; S tst1("node2")="value2" ;not used in json output+causes ugly json
  ; S tst1("node2",1)=3 ;indexes 1,2 is ignored, just values 3,4 are serialized
  ; S tst1("node2",2)=4
	;
	Q



; main entry point
; inmemory json serializer (limited by m string variable size)
; m wont accept subscripted variable as routine param
; so it passes as glvn string, eg. $$fn^module("subscriptedVarName")
stringify(glvn) ;
	new retVal,qarray,parent,qdeep,qstk,qcurr,qtype,qdata,qstop,qtemp;,qi
 	;new indent set indent=0 ;patch, indent is bad practice
	; perform some basic validation
	if $Get(glvn)="" Quit ""
	if glvn'?1A.AN.1(1"("1.ANP1")") Q ""

	; open root object or array
	set parent=glvn,qstop=1

	set qdeep=$QLength(parent) ;just seems stupid, json needs one root
	if (qdeep>1) Q ;todo: err, this version cant serialize when no single root

	; init state
	set retVal="" ;start object here
	set qarray=$Query(@parent)
	;set qarray=parent
	do QNode ;first pass for root node
	;w "retVal(",retVal,")"
	; navigate subscripts
	set qarray=glvn
	for  set qarray=$Query(@qarray) Q:qarray=""  do QArray Q:qarray=""
	; close any open objects and arrays
	set qdeep=-1
	do QClose
	; basic json string validation
	;if $Get(qi) S $ZTrap="ERR"
	; return json string
	Q retVal



; private fn
QArray:	;
	;W "QArray",!
	; get current subscript level
	;w "qarray(",qarray,")",!
	set qdeep=$QLength(qarray)
	;
	; close any open objects and arrays
	do QClose
	if qstk="" set qarray="" Quit
	;
	; now work through each subscript level
	set parent=qarray,qstop=0
	for qdeep=qstk:1:qdeep do
	. set parent=$Piece(qarray,",",0,qdeep)
	. if $Extract(parent,$Length(parent))'=")" set parent=parent_")"
	. ;w "parent(",parent,")",!
	. Do QNode
	. Q
	;
	Q



; private fn
QNode:	;
	; get current subscript value and type
	;w "qarray(",qarray,")",!
	;w "parent(",parent,")",!
	;w "qstop(",qstop,")"
	set qcurr=$QSubscript(qarray,qdeep)
	;numeric index = array
	set qtype=$Select($$IsNum(qcurr):"array",1:"object")
	;w "qcurr(",qcurr,")"
	;w "qtype(",qtype,")",!
	; if object member
	if qtype="object",'qstop set retVal=$$New(retVal,qcurr)
	; if array element
	if qtype="array",'qstop set retVal=$$New(retVal,"")
	; do we need to open an object or array?
	;w "$Data(@parent)=(",$Data(@parent),")",!
	;if ($Data(@parent)\10) do  Q
	if ($Data(@parent)\10) do  Q
	. ;w "sub(",$QSubscript(qarray,qdeep+1),")",!
	. ;w "sub(",$$IsNum($QSubscript(qarray,qdeep+1)),")",!
	. set qtype=$Select($$IsNum($QSubscript(qarray,qdeep+1)):"array",1:"object")
	. ;w "qarray(",qarray,")",!
	. ;w "child qtype(",qtype,")",!
	. if qtype="object" set retVal=$$Start(retVal,"{")
	. if qtype="array" set retVal=$$Start(retVal,"[")
	. ;set retVal=$$Start(retVal,"")
	. ;w "retVal(",retVal,")",!
	. set qstk(qdeep)=qcurr_"->"_qtype
	. Q
	; ensure array gaps are shown as empty elements
	if qtype="array",qcurr'>999 do
	. set qtemp=$Order(@qarray,-1)+1
	. for qtemp=qtemp:1:qcurr-1 set retVal=$$New(retVal_$Char(34,34),"")
	. Q
	; add data to string
	set qdata=@qarray
	if $$IsNum(qdata) set retVal=$$Number(retVal,qdata) Q
	if (qdata="true")!(qdata="false") set retVal=retVal_qdata Q
	set retVal=$$StrVal(retVal,qdata)
	;
	Q



;
Number:(json,num) ;
	;w "num(",num,")"
	Quit json_+num



; string serialization for name, must not be null
StrName:(json,str) ;
	;w "str(",str,")"
	if str="" Q json
	Q json_$Char(34)_$$Escape(str)_$Char(34)



; string serialization for value (can be null)
StrVal:(json,str) ;
	;w "str(",str,")"
	if str="" Q json_"null"
	Q json_$Char(34)_$$Escape(str)_$Char(34)



; private fn
Start:(json,str) ;
	;w "str(",str,")"
	;new char
	;if '$Data(ind) Quit json_str_" "
	;set char=$$Back(json,1)
	;if $Length(char),'(":"[char) set json=$$Pad(json,ind,$Char(13,10))
	;set ind=ind+2
	;Quit json_str_" "
	Q json_str



; private fn
New:(json,str) ;
	;w "New",!
	;w "str(",str'="",")",!
	new char
	set char=$$Back(json,0)
	;w "char(",char,")"
	;w "json(",json,")"
	;w "lenchar(",$Length(char),")"
	if $Length(char),'(char["{"),'(char["[") set json=json_","
	;if $Data(ind) set json=$$Pad(json,ind,$Char(13,10))
	if ($Data(str)&(str'="")) Q $$StrName(json,str)_":"
	Q json



; private fn
End:(json,str) ;
	;if $Data(ind) set json=json_$Char(13,10),ind=ind-2
	;Quit $$Pad(json,$Get(ind,1),"",str)
	Q json_str



; private fn
QClose:	;
	; close any open objects and arrays
	set qstk="",qstop=0
	for  set qstk=$Order(qstk(qstk),-1) Q:qstk=""  do  Q:qstop
	. if qstk'>qdeep,$Piece(qstk(qstk),"->")=$QSubscript(qarray,qstk) set qstk=qstk+1,qstop=1 Quit
	. if $Piece(qstk(qstk),"->",2)="object" set retVal=$$End(retVal,"}")
	. if $Piece(qstk(qstk),"->",2)="array" set retVal=$$End(retVal,"]")
	. kill qstk(qstk)
	. Q
	;
	Q



; private fn
Back:(str,pos) ;
	Q $Extract(str,$Length(str)-$Get(pos))



; private fn
Escape:(val) ;
	; http://www.json.org/
	;
	; " - quotation mark (34)
	; b - backspace (8)
	; f - formfeed (12)
	; n - newline (10)
	; r - carriage return (13)
	; t - horizontal tab (9)
	;
	new list
	for list="\\","//",$C(34)_$C(34),$C(8)_"b",$C(12)_"f",$C(10)_"n",$C(13)_"r",$C(9)_"t" do
	. for  Q:val=""  Q:val'[$E(list)  set val=$P(val,$E(list))_$C(2,3)_$P(val,$E(list),2,999)
	. if val[$C(3) set val=$TR(val,$C(2,3),"\"_$E(list,2))
	. Q
	;
	Q val



; private fn
IsNum:(value) ;
	Q value=+value



;not sure what is this for...
;StartObject(json,ind) ;
;	Quit $$Start(json,.ind,"{")
	;
;NewMember(json,ind,str) ;
;	Quit $$New(json,.ind,str)
	;
;EndObject(json,ind) ;
;	Quit $$End(json,.ind,"}")
	;
;StartArray(json,ind) ;
;	Quit $$Start(json,.ind,"[")
	;
;NewElement(json,ind) ;
;	Quit $$New(json,.ind)
	;
;EndArray(json,ind) ;
;	Quit $$End(json,.ind,"]")
	;
;Boolean(json,bool) ;
;	Quit json_$Select(bool:"true",1:"false")



test
  ;zlink "json/jsonStringify.m"
  S tst1("node1")="value1"
  ;S tst1("node2")="value2" ;not used in json, causes fail
  S tst1("node2",1)=3 ;index 1 is ignored
  S tst1("node2",2)=4
  W $$stringify^jsonStringify("tst1"),! ;variable pass as string!!
  ;result: { "node1": "value1", "node2": { 3, 4 } }

	S tst2(1)="bábovka"
	S tst2(2)="bublanina"
	S tst2(3)="ěščřžýáíéůúň"
	W $$stringify^jsonStringify("tst2"),!

	S tst3("n1")="bábovka"
	S tst3("n2",1)="bublanina"
	S tst3("n3",1)="ěščřžýáíéůúň"
	S tst3("n3",2)="kuuk"
	S tst3("n3",1,"s1")=1
	W $$stringify^jsonStringify("tst3"),!
  q
