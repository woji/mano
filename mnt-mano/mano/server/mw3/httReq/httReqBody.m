init() ; awport.pro 2022-01-21, please read LICENSE before use
  do import^runtime("string")
  ;do import^runtime("server/mw3/w3res")
  ;do import^runtime("server/mw3/urlRouter")
  Q



; loads http request body during client sock comm
; body is chunked into variable as array by 4000chars
Load(sockIO)
  D trace^runtime("D>w3reqBody.Load()")
  Q:('$data(W3JOB("HTTREQ","header","content-length")))
  ; -- decide how to read body, if any
  ;X:%WOS="CACHE" "U %WTCP:(::""S"")" ; Stream mode
  U sockIO:(nodelim) ; VEN/SMH - GT.M Delimiters
  if $$toLower^string($get(W3JOB("HTTREQ","header","transfer-encoding")))="chunked" D
  . do readChunks ; TODO: handle chunked input
  . if HTTPLOG>2 ; log array of chunks
  N %l S %l=$get(W3JOB("HTTREQ","header","content-length"),0)
  if (%l>0) do
  . do readBody(%l,99)
  . if HTTPLOG>2 D LOGBODY
  Q


;
readChunks ; todo:read body in chunks
 Q  ; still need to implement



;
readBody:(REMAIN,TIMEOUT) ; read L bytes with timeout T
  N X,LINE,LENGTH
  set LINE=0
RDLOOP: ;
  ; read until L bytes collected
  ; quit with what we have if read times out
  set LENGTH=REMAIN if LENGTH>4000 set LENGTH=4000
  read X#LENGTH:TIMEOUT
  if '$test do
  . ;D:HTTPLOG>1 LOGRAW("timeout:"_X)
  . set LINE=LINE+1,HTTPREQ("body",LINE)=X
  . S ^MW3S001TOBY("run","jobs",%id,"st")=0
  . S W3JOB("HTTSTAT","code")=400,W3JOB("HTTSTAT","msg")="invalid input stream"
  . Q ;invalid data when read
  ;if HTTPLOG>1 D LOGRAW(X)
  set REMAIN=REMAIN-$length(X),LINE=LINE+1,HTTPREQ("body",LINE)=X
  G:REMAIN RDLOOP
  Q



;
LOGBODY: ; log the request body
  Q:'$D(HTTPREQ("body"))
  N DT,ID
  set DT=HTTPLOG("DT"),ID=HTTPLOG("ID")
  if $get(NOGBL) Q
  M ^MW3S001TOBY("log",DT,$job,ID,"req","body")=HTTPREQ("body")
  Q
