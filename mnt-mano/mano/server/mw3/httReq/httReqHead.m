init() ; awport.pro 2022-01-21, please read LICENSE before use
  do import^runtime("string")
  ;do import^runtime("server/mw3/w3res")
  ;do import^runtime("server/mw3/urlRouter")
  Q



; loads http request header during client sock comm
Load(sockIO)
  D trace^runtime("httReqHead.Load()")
  N rawData
  ;setup sockIO params...
  ;todo: use UTF-8 !!
  U sockIO:(delim=$C(13,10):chset="M") ;just for sure...
  ;D trace^runtime("D>httReqHead:set delimiter for http ok, sockIO:"_sockIO)
  set rawData=$$readLine()
  ;
  if ('$L(rawData)) do  Q
  . D trace^runtime("D>httReqHead:header empty, bad req.!!")
  . S W3JOB("HTTSTAT","code")=400,W3JOB("HTTSTAT","msg")="invalid input stream"
  ;
  set W3JOB("HTTREQ","method")=$P(rawData," ")
  set W3JOB("HTTREQ","uri")=$P(rawData," ",2)
  set W3JOB("HTTREQ","proto")=$P(rawData," ",3)
  set W3JOB("HTTREQ","uri","path")=$P(W3JOB("HTTREQ","uri"),"?")
  set W3JOB("HTTREQ","uri","query")=$P(W3JOB("HTTREQ","uri"),"?",2,999)
  ; TODO: time out connection after N minutes of wait
  ; TODO: check format of rawData and raise error if not correct
  if ($extract($piece(rawData," ",3),1,4)'="HTTP") do  Q
  . D trace^runtime("D>httReqHead:HTTERR400 end")
  . S W3JOB("HTTSTAT","code")=400,W3JOB("HTTSTAT","msg")="invalid header"
  ; -- read the rest of the lines in the header (empty line=end of head)
  for  set rawData=$$readLine() Q:'$length(rawData)  do addHead(rawData)
  ;if (W3JOB("HTTERR","code")=0)
  ;S W3JOB("HTTERR","code")=0,W3JOB("HTTERR","msg")=""
  D trace^runtime("D>httReqHead.Load() exit ok")
  Q ; ok httpReqHeader



loadRawAll:()
  N rawData S rawData=$$readLine()
  ;N retV,%d S retV=""
  ;for  read %d S retV=retV_%d Q:($A($ZB)=13)!(%d="")
  S W3JOB("HTTREQ","method")="GET",W3JOB("HTTREQ","uri","path")="/"
  S W3JOB("HTTSTAT","code")=404,W3JOB("HTTSTAT","msg")="method not found"
  D trace^runtime("D>httReqHead rawData:"_rawData)
  Q



;
readLine:() ; read a header line
  ; fixes a problem where the read would terminate before CRLF
  ; (on a packet boundary or when 1024 characters had been read)
  D trace^runtime("D>httReqHead.readLine()")
  N retV,%d S retV="",%d=""
  for  read:10 %d S retV=retV_%d Q:($A($ZB)=13)!(%d="")!('$T)
  D trace^runtime("D>httReqHead readLine:"_retV)
  Q retV
  ;N X,LINE,RETRY
  ;set LINE=""
  ;F RETRY=1:1:10 R X:10 S LINE=LINE_X Q:$A($ZB)=13
  ;Q LINE
  ;for RETRY=1:1:10 Q:$A($ZB)=13  do
  ;. read X:10 ;timeout 10s
  ;. D trace^runtime("D>httReqHead.readLine:"_X)
  ;. set LINE=LINE_X
  ;Q LINE



;todo:need test bottom 4 lines looks very susp.
addHead:(LINE) ; add header name and header value
 ; expects httReqHead to be defined
 ;do:HTTPLOG LOGHDR(LINE)
 N NAME,VALUE
 set NAME=$$toLower^string($$ltrim^string($piece(LINE,":")))
 set VALUE=$$ltrim^string($piece(LINE,":",2,99))
 if LINE'[":" set NAME="",VALUE=LINE
 if '$length(NAME) set NAME=$get(W3JOB("HTTREQ","headers")) ; grab the last name used
 if '$length(NAME) Q  ; no header name so just ignore this line
 D trace^runtime("D>httReqHead.addHead("_NAME_","_VALUE_")")
 if $D(W3JOB("HTTREQ","headers",NAME)) D
 . set W3JOB("HTTREQ","headers",NAME)=W3JOB("HTTREQ","headers",NAME)_","_VALUE
 else  do
 . set W3JOB("HTTREQ","headers",NAME)=VALUE,W3JOB("HTTREQ","headers")=NAME
 ;input has location header
 S:(NAME="location") W3JOB("HTTSTAT","code")=201,W3JOB("HTTSTAT","msg")=""
 Q
