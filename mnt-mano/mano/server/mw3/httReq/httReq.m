init() ; awport.pro, 2022-01-21, please read LICENSE before use
  do import^runtime("server/mw3/httReq/httReqHead")
  do import^runtime("server/mw3/httReq/httReqBody")
  do import^runtime("server/mw3/urlRouter")
  Q



;
Load(sockIO)
  D trace^runtime("httReq.Load()")
  K W3JOB("HTTREQ")
  ;D loadRawAll()
  ;Q
  D Load^httReqHead(.sockIO) ;try load reqHeader
  Q:(W3JOB("HTTSTAT","code")>399)
  D trace^runtime("D>httReq method:"_W3JOB("HTTREQ","method"))
  N %me,%ph S %me=W3JOB("HTTREQ","method"),%ph=W3JOB("HTTREQ","uri","path")
  if ('$$PathHasMethod^urlRouter(W3JOB("instance"),%ph,%me)) do
  . S W3JOB("HTTSTAT","code")=404,W3JOB("HTTSTAT","msg")="method "_%me_" for path "_%ph_" not found"
  ; reading beyond last data causes job stop !!!
  ;Q
  ;look for error
  ;if (W3JOB("HTTSTAT","code")>399) do  Q
  ;. D trace^runtime("D>httReq begin read to end")
  ;. ;read all stream data and quit
  ;. for  read %d Q:($A($ZB)=13)!(%d="")  D trace^runtime("D>httReq readed to end of data"_$A(%d))
  ;. D trace^runtime("D>httReq read to end done")
  ;D trace^runtime("D>httReq read after to end")
  ;post put and patch can have body
  ;todo:load body always but put to null when not POST, PUT, PATCH
  if (%me="POST"!%me="PUT"!%me="PATCH") do Load^httReqBody(.sockIO)
  ;else  for  read %d Q:($A($ZB)=13)!(%d="")  D trace^runtime("D>httReq readed to end of data"_$A(%d))
  Q



;for debuging purposes only
;loadRawAll:()
;  N retV,%d S retV=""
;  for  read %d S retV=retV_%d Q:($A($ZB)=13)!(%d="")
;  S W3JOB("HTTREQ","method")="GET",W3JOB("HTTREQ","path")="/"
;  S W3JOB("HTTSTAT","code")=404,W3JOB("HTTSTAT","msg")="method not found"
;  D trace^runtime("D>httReq rawData:"_retV)
;  Q
