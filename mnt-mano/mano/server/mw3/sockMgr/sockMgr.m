; 2022-01-31 awport.pro, please read LICENSE before use
; socket device mgmt for mw3 and w3job
init()

  Q



;open socket device
OpenSOC(devName)
  ;without command (CONNECT,LISTEN,..) it just open sock device
  open devName:::"SOCKET"
  else  D trace^runtime("D>sockMgr err:cant open socket device:"_devName)
  D trace^runtime("D>sockMgr.openSOC "_devName_" done")
  Q



;
OpenListenTcpSOC(dev,attchId,port,queueLen)
  D trace^runtime("D>sockMgr.openListenTcpSOC:open "_dev_":(LISTEN="""_port_":TCP"":delim=$C(13,10):attach="_attchId_"):15:""SOCKET""")
  open dev:(LISTEN=port_":TCP":delim=$C(13,10):attach=attchId):15:"SOCKET" ;
  else  D trace^runtime("D>sockMgr err: cant open tcp:"_port) Q
  ;use dev:(CHSET="M")
  ;set listen with TCP queue lenght=5
  D trace^runtime("D>sockMgr set queue len:"_queueLen)
  W /LISTEN(queueLen) ;sets $KEY to "LISTENING|socket_handle|portnumber"
  D trace^runtime("D>sockMgr.openListenTcpSOC done")
  Q



;open local socket as CONNECT
OpenConnectLSOC(dev,sockId,attchId)
  D trace^runtime("D>sockMgr.openConnectLSOC("_dev_","_sockId_","_attchId_")")
  open dev:(CONNECT=sockId_":LOCAL":ATTACH=attchId)::"SOCKET"
  ;open dont set $test when no timeout specified like ... :1:"SOCKET"
  ;else  D trace^runtime("sockMgr err:cant open local connected:"_sockId) Q
  D trace^runtime("D>sockMgr.openConnectLSOC ok")
  Q



;open local socket as LISTEN
OpenListenLSOC(dev,sockId,attchId)
  D trace^runtime("D>sockMgr.openListenLSOC("_dev_","_sockId_","_attchId_")")
  ;open "SOC":(LISTEN="w3jobCommSck1:LOCAL":ATTACH="w3jobCommAttch1")::"SOCKET"
  D trace^runtime("D>sockMgr open "_dev_":(LISTEN="_sockId_":LOCAL"":ATTACH="_attchId_")::""SOCKET""")
  open dev:(LISTEN=sockId_":LOCAL":ATTACH=attchId)::"SOCKET"
  ;open dont set $test when no timeout specified
  ;else  D trace^runtime("sockMgr err:cant open local listen "_sockId) Q
  ;U dev
  ;U dev:(attach=sockId)
  D trace^runtime("D>sockMgr.openListenLSOC ok")
  Q



;closes requested socket held by device
CloseSOCS(dev,attchId)
  D trace^runtime("D>sockMgr.CloseSOCS("_dev_","_attchId_")")
  C dev:(socket=attchId)
  ;close dont set $test :(
  ;else  D trace^runtime("sockMgr err: socket "_attchId_" close failed !!")
  Q



;close device
CloseSOC(dev)
  D trace^runtime("D>sockMgr.CloseSOC("_dev_")")
  C dev
  ;close dont set $test :(
  ;else  D trace^runtime("sockMgr err: close failed !!")
  Q
