init() ; awport.pro 2022-01-21, please read LICENSE before use

  Q



; adds m routine to MW3 routing map
; w/ params:
; urlPath - url to accept by MW3
; httMeth - GET,POST,PUT,...
; rtnNS - routine namespace
; rtnName - m routine name, expected routine signature is xx^xx(ioId)
;  where ioId is name of opened IO device to stream result data into
; cant use path as subscript, max var name incl subscript is 255chars
; path can be longer
;S ^MW3S001TOBY(instName,"routes",0,"path")="/apples/add"
;S ^MW3S001TOBY(instName,"routes",0,"method","POST")="routine^name"
AddRoute(instName,urlPath,httMeth,rtnNS,rtnName)
  D trace^runtime("D>urlRouter.addRoute("_instName_","_urlPath_","_httMeth_","_rtnName_")")
  ;validate method names
  N %hm S %hm="|GET|HEAD|POST|PUT|DELETE|CONNECT|OPTIONS|TRACE|PATCH|"
  if '(%hm[httMeth) do  Q
  . D trace^runtime("urlRouter err: unknown http method:"_httMeth)
  ;validate path and routine
  if (urlPath=""!rtnName="") do  Q
  . D trace^runtime("urlRouter err: urlPath or rtnName empty, not added")
  ;probe routine available
  ;makes no sense in development mode, as routine is not available to jobs process
  ;if ($text(@rtnName)="") D trace^runtime("urlRouter err: routine "_rtnName_" not found") Q
  ;add
  D trace^runtime("D>urlRouter add")
  S rtnNS=$get(rtnNS,"")
  N %ix S %ix=$get(^MW3S001TOBY(instName,"routes"),0)
  S ^MW3S001TOBY(instName,"routes")=$incr(%ix)
  S ^MW3S001TOBY(instName,"routes",%ix,"path")=urlPath
  S ^MW3S001TOBY(instName,"routes",%ix,"method",httMeth)=rtnNS_"|"_rtnName
  ;S ^MW3S001TOBY(instName,"routes",%ix,"target")=rtnName
  D trace^runtime("D>urlRouter add done")
  Q



;proces registered routes in S ^MW3S001TOBY(instName,"routes",0..n)
;sets one param to routine (ioID)
Route(instName,urlPath,httMeth,ioID)
  D trace^runtime("urlRouter.Route("_urlPath_","_httMeth_","_ioID_")")
  N %rtn S %rtn=$$FindRoute(instName,urlPath,httMeth)
  ;
  if (%rtn="") do  Q
  . S W3JOB("HTTERR","code")=404,W3JOB("HTTERR","msg")="path "_urlPath_" not found"
  ;
  D trace^runtime("D>routine:"_%rtn_" found for url:"_urlPath)
  ;is namespace import specified ?
  ;eg. myw3svc/myw3svcCtrlr|Routine1^myw3svcCtrlr
  N %ns S %ns=$piece(%rtn,"|",1)
  S %rtn=$piece(%rtn,"|",2)
  ;if (%rtn="") S %rtn=%ns,%rtn="D "_%rtn_"("""_ioID_""")"
  ;else  S %rtn="D i^runtime("""_%ns_""") D "_%rtn_"("""_ioID_""")"
  ;D r^runtime(%ns,%rtn,ioID)
  ;
  ;S %rtn="D "_%rtn_"("""_ioID_""")"
  ;call it
  D trace^runtime("D>routine call:"_%rtn)
try1: S $ETRAP="goto catch1^urlRouter"
  D r^runtime(%ns,%rtn,ioID)
  D trace^runtime("D>urlRouter Route ok")
  G finally1
catch1 N %EX S %EX=$ZSTATUS ;public!!
  D trace^runtime("urlRouter route exception:"_%EX)
  S W3JOB("HTTERR","code")=500,W3JOB("HTTERR","msg")=%EX
  S $ECODE="" ;reset err
finally1:
  D trace^runtime("D>urlRouter done")
  S $ETRAP="Q"
  Q



; returns index of route by path and method
; this is helper for w3req
FindRoute(instName,urlPath,httMeth)
  D trace^runtime("D>urlRouter.FindRoute("_urlPath_","_httMeth_")")
  N retV,%i,%p S retV=""
  for %i=-1:0 S %i=$O(^MW3S001TOBY(instName,"routes",%i)) Q:((%i="")!(retV'=""))  do
  . S %p=$get(^MW3S001TOBY(instName,"routes",%i,"path"),""),retV=$get(^MW3S001TOBY(instName,"routes",%i,"method",httMeth),"")
  . D trace^runtime("D>urlRouter path&routine:"_%p_","_retV)
  . ;case sensitive !!
  . ;if (%p=urlPath&retV'="") Q retV
  Q retV



;
PathHasMethod(instName,urlPath,httMeth)
  D trace^runtime("D>urlRouter.PathHasMethod("_urlPath_","_httMeth_")")
  N %r S %r=$$FindRoute(instName,urlPath,httMeth)
  D trace^runtime("D>urlRouter PathHasMethod exit with:"_$L(%r))
  Q $L(%r)



;;
;parseQstr:(urlQry,QUERY) ; parses and decodes query fragment into array
;  ; expects QPARAMS to contain "query" node
;  ; .QUERY will contain query parameters as subscripts: QUERY("name")=value
;  ;todo: parse value as array
;  N %i,%X,NAME,VALUE
;  for %i=1:1:$length(urlQry,"&") D
;  . S %X=$$URLDEC($piece(urlQry,"&",%i))
;  . S NAME=$piece(%X,"="),VALUE=$piece(%X,"=",2,999)
;  . if $length(NAME) S QUERY($zconvert(NAME,'U')=VALUE
;  Q



;
;URLDEC(X,PATH) ; Decode a URL-encoded string
;  ; Q $ZCONVERT(X,"I","URL")  ; uncomment for fastest performance on Cache
;  ;
;  N %i,OUT,FRAG,ASC
;  S:'$G(PATH) X=$TR(X,"+"," ") ; don't convert '+' in path fragment
;  F %i=1:1:$L(X,"%") D
;  . I %i=1 S OUT=$P(X,"%") Q
;  . S FRAG=$P(X,"%",%i),ASC=$E(FRAG,1,2),FRAG=$E(FRAG,3,$L(FRAG))
;  . I $L(ASC) S OUT=OUT_$C($$HEX2DEC(ASC))
;  . S OUT=OUT_FRAG
;  Q OUT
