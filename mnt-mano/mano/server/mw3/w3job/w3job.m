init() ; awport.pro 2022-01-21, please read LICENSE before use
; ------------- http request/response job Handling Process --------------
; job takes care of comm w/ MW3 and proper states
; and handles incomming http request
; todo: cleanup SOC use !!
  D import^runtime("datetime")
  D import^runtime("server/mw3/httReq")
  D import^runtime("server/mw3/httRes")
  D import^runtime("server/mw3/sockMgr")
  D import^runtime("server/mw3/urlRouter")
  Q



;
; handle HTTP requests on this connection
; as new job (process) spawned from MW3
StartJob(INSTID,ID)
  ; debug instrumentation
  S $ZYERROR="errReport^runtime"
  O ^mlangRuntime("config","trace","outTerm")
  ;initialize job
  D trace^runtime("w3job.StartJob("_$get(ID,"")_"), PID:"_$job_",$PRINCIPAL:"_$PRINCIPAL)
  if ('$data(ID)) D trace^runtime("w3job ID missing, quit") Q
  if ('$data(W3JOB)) do init() do defaults ;ensure that init is called first time
  ;D trace^runtime("D>$IO:"_$IO)
  S W3JOB=ID ;job id
  S SVRID=INSTID ; server instance job belong to
  ;N %startT,%endT S %startT=$horolog,%endT=$horolog
  ;turn state to running
  S ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"st")=1 ;-1=err,0=new,1=running,2=wait4job
  S ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"pid")=$job ; this is for W /PASS
  S ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"instance")=INSTID ; instance id
  ;S ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"sock")=$PRINCIPAL ;is open
  ;copy jobs data into mem var (avoid disk & long var name)
  M W3JOB=^MW3S001TOBY(SVRID,"run","jobs",W3JOB)
  ;current process stdin,stdout&stderr is SOCket device
  ;use SOC as device id everywhere
  ;S SOC=W3JOB("sock")
  S SOC=$PRINCIPAL
  ;this time override socket with $PRINCIPAL
  ;S W3JOB("sock")=$PRINCIPAL,
  D trace^runtime("D> w3job #"_W3JOB_"st:"_W3JOB("st")_",SOC:"_SOC_", tcpSockHndl:"_W3JOB("sock"))
  ;S:('$data(SOC)) SOC="SCK$"_^MW3S001TOBY(SVRID,"config","port")
  ;S W3JOB("sock")=$GET(SOC,$PRINCIPAL) ; m job call passed TCP Device
  ;D trace^runtime("D>W3JOB(tcpSock):"_W3JOB("sock"))
  ;X "U 0:(delim=$C(13,10):chset=""M"")"
  U SOC:(delim=$C(13,10):chset="M")
  ;D trace^runtime("D>w3job.debug line 37 W HTTP/1.1")
  ;W "HTTP/1.1 200 OK"_$C(13,10),!
  ;W "HTTP/1.1"
  ;D trace^runtime("D>setup SOC ok:")
  N %jobStop S %jobStop=0 ;false, update in loop from ^MW3S001TOBY(SVRID,"run","mw3","st")
  ; <debug instrumentation
  ;N %debugcycles S %debugcycles=20
  ;N %dStartT,%dEndT S %dStartT=$horolog,%dEndT=%dStartT
  ; end debug instrumentation>
  ; ------ job loop ------
  for  Q:(%jobStop)  do
  . ; if running state or sockOpen, process req, then set wait state
  . ;D trace^runtime("D>w3job:KEY:"_$KEY_",device:"_$device_",test:"_$test)
  . ; <debug instrumentation
  . ;S %debugcycles=%debugcycles-1 S:('%debugcycles) ^MW3S001TOBY(SVRID,"run","mw3","st")="stop"
  . ;if ($$DateDiff^datetime(%dStartT,%dEndT,"S")>30) do ;stop after 30 sec.
  . ;. S ^MW3S001TOBY(SVRID,"run","mw3","st")="stop"
  . ;S %dEndT=$horolog
  . ;end debug instrumentation>
  . D:((W3JOB("st")=1)&(W3JOB("sock")'="")) procReq()
  . ;otherwise do job maintenance
  . S %jobStop=$$w3JobMaint()
  . hang 0.1 ;slow it down in development stage
  . ;each 5secs check run status
  . ;if ($$DateDiff^datetime(%startT,%endT,"S")>5) do
  . ;. D updJobStat("") S %startT=$horolog
  . ;S %endT=$horolog hang 2 ;relax
  . D trace^runtime("D>w3job #"_W3JOB_" loop,st:"_W3JOB("st"))
  ; ------ job loop end ------
  close SOC
  H



; ------- socket open, must process http request -------
procReq:()
  D trace^runtime("w3job.procReq(),st:"_W3JOB("st"))
  S W3JOB("HTTSTAT","code")=200,W3JOB("HTTSTAT","msg")="" ;reset when req start
  ; parse req header&body
  do Load^httReq(SOC)
  ; prep. response
  ;do:(W3JOB("HTTSTAT","code")<400) New^httRes(SOC)
  ; if no err, try route to execute routine
  ; registered in routine map
  if (W3JOB("HTTSTAT","code")<400) do
  . D Send^httRes(SOC)
  . D sleep()
  ;
  D trace^runtime("D>w3job #"_W3JOB_" W3JOB(HTTSTAT,code):"_W3JOB("HTTSTAT","code"))
  ;
  if (W3JOB("HTTSTAT","code")>399) do  Q
  . ;http error causes sleep (stream close)
  . D SendErr^httRes(SOC,W3JOB("HTTSTAT","code"),W3JOB("HTTSTAT","msg"))
  . D sleep()
  ;if routed ok but no output, make HTTP200 with no data
  if (W3JOB("HTTSTAT","response")'="done") do
  . ;HTTPSTAT!=20X
  S W3JOB("sockCloseTout")=$horolog
  D updJobStat("") ;just update W3JOB("jobUpdTime")
  Q



;
w3JobMaint:()
  D trace^runtime("D>w3job.w3JobMaint()")
  N %now,%sto S %now=$horolog,%sto=$get(W3JOB("sockCloseTout"),%now)
  ;;I'am alive
  ;S ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"jobUpdTime")=%now ; job is alive
  ; not used for 5sec and socket open ?
  if (W3JOB("st")=1&$$DateDiff^datetime(%sto,%now,"S")>5) D sleep()
  ; sleeping and asked to process req ?
  else  if ((W3JOB("st")=3)&(^MW3S001TOBY(SVRID,"run","jobs",W3JOB,"st")=7)) D
  . M W3JOB=^MW3S001TOBY(SVRID,"run","jobs",W3JOB)
  . S W3JOB("sock")=$$awaitNewTcp(),W3JOB("st")=1
  . D updJobStat("")
  . D procReq(); do immediate
  else  if ($$DateDiff^datetime(W3JOB("jobUpdTime"),%now,"S")>5) D updJobStat("")
  ; quit w/ mw3 server state
  D trace^runtime("D>w3job.w3JobMaint done")
  Q $get(^MW3S001TOBY(SVRID,"run","mw3","st"))="stop"



;
awaitNewTcp:()
  D trace^runtime("D>w3job.awaitNewTcp()")
  ;todo: when SOC=$principal, stop using it now
  ; and open new SOC for all future requests
  ;but still one connection remains opened as close don't work on $principal
  if (SOC=$PRINCIPAL) D
  . S SOC="SOC" D OpenSOC^sockMgr(SOC)
  ;
  N LOC S LOC="LOC"
  D OpenSOC^sockMgr(LOC)
  ;D OpenSOC^sockMgr(SOC)
  D OpenListenLSOC^sockMgr(LOC,"commSockId"_W3JOB,"commSocAttchId"_W3JOB)
  use LOC:(socket="commSocAttchId"_W3JOB) ;just for sure
  ;update global job status to ready for new tcp socket
  D trace^runtime("D>w3job #"_W3JOB_" set to st:15")
  S W3JOB("st")=15 M ^MW3S001TOBY(SVRID,"run","jobs",W3JOB)=W3JOB
  W /wait ;stop here until connected from mw3
  D trace^runtime("D>w3job #"_W3JOB_" KEY:"_$KEY)
  ;hang 0.3
  N handle S handle=""
  D trace^runtime("D>w3job #"_W3JOB_" W /ACCEPT")
  ;W /ACCEPT(.handle,,,"handle")
  W /accept(.handle,,,W3JOB("sock"))
  else  D trace^runtime("D>w3job #"_W3JOB_":err accept tcp socket")
  D trace^runtime("D>w3job #"_W3JOB_" W /ACCEPT done")
  D trace^runtime("D>w3job #"_W3JOB_" U s ..attach")
  use SOC:(attach=handle)
  U SOC:(delim=$C(13,10):chset="M")
  ;C LOC
  D CloseSOCS^sockMgr(LOC,"commSocAttchId"_W3JOB)
  D CloseSOC^sockMgr(LOC)
  ;D trace^runtime("D>sub:write /wait")
  U SOC:(delim=$C(13,10):chset="M")
  Q handle



;put job state into wait for req state
sleep:()
  D trace^runtime("w3job.sleep()")
  D:(W3JOB("sock")'="") CloseSOCS^sockMgr(SOC,W3JOB("sock"))
  ;D:(W3JOB("sock")'="") CloseSOC^sockMgr(SOC)
  S W3JOB("st")=3,W3JOB("sock")=""
  D updJobStat("")
  ;if (W3JOB("sock","st")=1) do closeSock()
  ;S W3JOB("st")=3 M ^MW3S001TOBY(SVRID,"run","jobs",W3JOB)=W3JOB
  Q



;sync state JOB > global
; param action is unused Now
;in future it might be "in" or "out"
; to let one routine sync JOB > global and JOB < global
updJobStat:(action)
  D trace^runtime("D>w3job #"_W3JOB_" updJobStat, st:"_W3JOB("st")_",action:"_action)
  ;if (action="sleep") D
  ;. D:(W3JOB("sock")'="") CloseSOCS^sockMgr(SOC,W3JOB("sock"))
  ;. S W3JOB("st")=3,W3JOB("sock")=""
  ;else  if (action="awaitNewTcp") D
  ;. S W3JOB("st")=15,M ^MW3S001TOBY(SVRID,"run","jobs",W3JOB)=W3JOB
  ;. W3JOB("sock")=$$awaitNewTcp(),W3JOB("st")=1
  ;I'am alive
  S W3JOB("jobUpdTime")=$horolog
  ;todo: might cause problems when wrong programmed
  M ^MW3S001TOBY(SVRID,"run","jobs",W3JOB)=W3JOB
  D trace^runtime("D>w3job updJobStat done")
  Q



;
defaults:
  ;see RunJob(ID,...)
  ;and use as ^MW3S001TOBY(SVRID,"run","jobs",W3JOB,...)
  S W3JOB=-1
  S SVRID=""
  S %WOS=$$engine^runtime()
  S SOC=""
  ;set %WOS=$S($P($SY,",")=47:"GT.M",$P($SY,",")=50:"MV1",1:"CACHE") ; Get Mumps Virtual Machine
  Q



;err handler
;errReport
;  ;s $ETRAP="Q"
;  D trace^runtime("D>err")
;  D trace^runtime($ZSTATUS)
;  D trace^runtime($ZERROR)
;  Q
