# MW3 Server - MUMPS WWW server
current version 0.0.1, codename TOBY
This source tree represents a web (HTTP) server implemented in the M language.
This code is precursor for future pure tcp MUMPS server (GTTP), generic TCP trafic processor

## Purpose
missing easy to read & customize www server in M, node.js alike
M is strongly I/O tied lang, but no useful FOSS  www server is available
original MWS is not only compressed source extremely hard to read code but also obsolete architecture, where each request spawns new process.
MW3 server uses process pool of defined size and each process is recycled for new http request

## code writing rules
shortened command upper case like N (new command)
full name command is alllower case like write /WAIT
never make variable name of 1 char named as command !!! at least 2 chars like %D
private module functions camelCase
public functions CapitalCase

common variable camelCase
always use self explaining names
global variable CapitalCase
constanst UPPERED

to sustain code readability when project grow, following rules applied:
  split code into files and subfolders by its domains
  allow for easy attach set of functions to program

# runtime.m
is main entry point of whole "framework"
```
do import^runtime("path/to/library") is component loader
```
this makes zlink+runs init label within zlinked routine module, which is recursive

2 routine file types:
  library - links multiple modules in one set of functions (todo:autogenerate lib files)
  module - single function


## MW3 server code
MW3 uses global var named ^MW3[major][minor][patch][codename] eg. ^MW3S001TOBY.
this variable is used for config, runtime and interprocess communication
variable name will change with versions
