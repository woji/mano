init() ; awport.pro 2022-01-21, please read LICENSE before use
  do import^runtime("datetime")
  do import^runtime("stream/bufferWriter")
  do import^runtime("server/mw3/res/httStatCode")
  ;do import^runtime("server/mw3/urlRouter")
  Q



; create http header during client sock comm
;Create()
;  ;todo:not implemented
;  D trace^runtime("D>w3resHead.Create()")
;  N rawData ;inmem var for 1MB output (enough for header)
;  D trace^runtime("D>w3resHead.Create() exit ok")
;  Q ; ok httpReqHeader



;transport head sets Content-Type: Content-Length:,...
addTransport:(contType,contSize)
  D WriteBuff^bufferWriter(sockIO,"Content-Type: "_contType_$C(13,10))
  D WriteBuff^bufferWriter(sockIO,"Content-Length: "_contSize_$C(13,10))
  Q



; general header data
addGeneral:()
  D WriteBuff^bufferWriter(sockIO,"Date: "_$$GMT^datetime_$C(13,10)) ; RFC 1123 date
  D WriteBuff^bufferWriter(sockIO,"Server: "_$$serverName_$C(13,10)) ;
  D WriteBuff^bufferWriter(sockIO,"Connection: Closed"_$C(13,10))
  Q



;create html header for HTTPERR output
CreateErr(sockIO,code,msg,contType,contSize)
  D trace^runtime("D>w3resHead.CreateErr("_code_","_msg_")")
  D WriteBuff^bufferWriter(sockIO,$$GetResStatLine^httStatCode(code,msg)_$C(13,10))
  D addGeneral()
  D addTransport(contType,contSize)
  ;message ends with free line
  D WriteBuff^bufferWriter(sockIO,$C(13,10))
  D trace^runtime("D>w3resHead.CreateErr done")
  Q



CreateOk(sockIO,code,msg,contType,contSize)
  D trace^runtime("D>w3resHead.CreateOk("_code_","_msg_")")
  D WriteBuff^bufferWriter(sockIO,$$GetResStatLine^httStatCode(code,msg)_$C(13,10))
  D addGeneral()
  D addTransport(contType,contSize)
  ;message ends with free line
  D WriteBuff^bufferWriter(sockIO,$C(13,10))
  D trace^runtime("D>w3resHead.CreateOk done")
  Q



;temporary
serverName:()
  Q "yottadb MW3 Server (Linux)"
