init() ; awport.pro, 2022-01-21, please read LICENSE before use
; in case of server overhead, this module is also loaded/called
; from main MW3 process, just to make HTTP503 refused
;
  ;do import^runtime("json")
  do import^runtime("server/mw3/res/httStatCode")
  do import^runtime("server/mw3/httRes/httResHead")
  do import^runtime("server/mw3/res/httErrPage")
  do import^runtime("stream/bufferWriter")
  Q



;
new:(sockIO)
  D trace^runtime("D>httRes.New()")
  S W3JOB("HTTSTAT","response")=""
  Q



;create and send http error response
SendErr(sockIO,code,msg)
  D trace^runtime("httRes.SendErr("_code_","_msg_")")
  D new(.sockIO)
  ;N sockIO S sockIO=$principal
  do Open^bufferWriter(sockIO,,"SOC")
  ;get err page (must fit into 1MB)
  N %htm,contSZ S %htm=$$GetBody^httErrPage(code,msg),contSZ=$zlength(%htm)
  ;all output buffered, even head can be large
  D CreateErr^httResHead(sockIO,code,msg,"text/html; charset=UTF-8",contSZ)
  D WriteBuff^bufferWriter(sockIO,%htm)
  ;I 'SIZE!(W3JOB("HTTREQ","method")="HEAD") D FLUSH Q
  ;do trace^runtime(%WBUFF)
  D Close^bufferWriter(sockIO)
  S W3JOB("HTTSTAT","response")="done"
  Q



;try create and send response
Send(sockIO)
  D trace^runtime("D>httRes.Send()")
  D new(.sockIO)
  ;create memory writer for data
  N ioID S ioID="::MEM::"
  D Open^bufferWriter(ioID,,"MEM")
  ;do Open^bufferWriter("mem",,"memory")
  ;get output data (must fit into 1MB)
  ;Route output is either string or file or subscribed variable with strings eg (0,"") (1,"")
  D Route^urlRouter(W3JOB("instance"),W3JOB("HTTREQ","uri","path"),W3JOB("HTTREQ","method"),ioID)
  N %ct,contSZ,%data S %ct=$$ContentType^bufferWriter(ioID)
  if (%ct="string") do
  . S %data=$$ToString^bufferWriter(ioID)
  . S contSZ=$zlength(%data)
  ;
  D Close^bufferWriter(ioID)
  ;
  ;now open writer on socket and write all to output
  do Open^bufferWriter(sockIO,,"SOC")
  ;all output buffered, even head can be large
  N code,msg S code=200,msg="OK"
  ;unset delimiter before output !!!
  U SOC:(NODELIM:chset="UTF-8")
  D CreateOk^httResHead(sockIO,code,msg,"text/html; charset=UTF-8",contSZ)
  D WriteBuff^bufferWriter(sockIO,%data)
  ;I 'SIZE!(W3JOB("HTTREQ","method")="HEAD") D FLUSH Q
  ;do trace^runtime(%WBUFF)
  D Close^bufferWriter(sockIO)
  S W3JOB("HTTSTAT","response")="done"
  D trace^runtime("httRes Send done")
  Q


;
defaults:()

  Q
