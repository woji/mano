init() ; awport.pro 2022-01-21, please read LICENSE before use
  do import^runtime("server/mw3/sockMgr")
  do import^runtime("server/mw3/httRes")
  do import^runtime("datetime")
  Q
;to run app:
;source /opt/yottadb/current/ydb_env_set && export ydb_routines="/mano $ydb_routines" && /opt/yottadb/current/yottadb -dir
;D setTrace^runtime("D",$principal) job rj^runtime("server/mw3","Start^mw3","test1",8080,5,5)
;test app using curl like:
;curl -X GET http://awport.pro:9080
;emergency stop
;W "404" C SOC U 0 S ^MW3S001TOBY("run","mw3","st")="stop"



;declarative add/register named server instance
Add(svrInstId)
  D trace^runtime("mw3.Add("_$get(svrInstId,"")_")")
  if ('$data(MW3S001TOBY)) do defaults()
  if ($data(^MW3S001TOBY(svrInstId))) do  Q
  . D trace^runtime("mw3 err:instance "_$get(svrInstId,"")_" already exist, end")
  S ^MW3S001TOBY(svrInstId)=svrInstId
  Q



;removes named instance
Delete(svrInstId)
  K ^MW3S001TOBY(svrInstId)
  Q



;starting with defaults
Start(svrInstId,TCPPORT,MAXJOBS,TCPQLEN) ; set up listening for connections
  if ('$data(MW3S001TOBY)) do defaults() ;when direct call do Start^mw3
  K ^MW3S001TOBY(svrInstId)
  D Add(svrInstId)
  D trace^runtime("mw3.Start("_svrInstId_","_$get(TCPPORT,"")_","_$get(MAXJOBS,"")_","_$get(TCPQLEN,"")_")") ;runtime will load
  D Listen(.svrInstId,.TCPPORT,.MAXJOBS,.TCPQLEN)
  Q



;Listen expect that all is preconfigured in ^MW3S001TOBY and just runs on it
; params:
;  TCPPORT is listening port number
;  MAXJOBS - max number of processes
;  TCPQLEN - size of system socket cache (waiting and not refused)
Listen(svrInstId,TCPPORT,MAXJOBS,TCPQLEN) ; set up listening for connections
  S $ZYERROR="errReport^runtime"
  S $ETRAP="Q"
  ;
  if ('$data(MW3S001TOBY)) do defaults() ;when direct call do Start^mw3
  ;
  D trace^runtime("mw3.Listen("_svrInstId_","_$get(TCPPORT,"")_","_$get(MAXJOBS,"")_","_$get(TCPQLEN,"")_"), pid:"_$job) ;runtime will load
  if ('$data(^MW3S001TOBY)) do  Q
  . D trace^runtime("mw3 config missing, either use Start or configure server first, end now...")
  ;just foo now to recognize if defaults() was called
  ;future version will use it more
  M MW3S001TOBY("config")=^MW3S001TOBY("config")
  ;todo: resolve previous for recovery when server fails
  D resetJobs(svrInstId) ;this first!!!
  ;D trace^runtime("mw3.Start("_$get(TCPPORT,"")_","_$get(MAXJOBS,"")_","_$get(TCPQLEN,"")_"), PID:"_$job) ;runtime will load
  ;
  D:('$data(TCPPORT)) trace^runtime("mw3:no TCPPORT specified, using:"_^MW3S001TOBY("config","tcpPort"))
  D:('$data(MAXJOBS)) trace^runtime("mw3:no MAXPROC specified, using:"_^MW3S001TOBY("config","maxJobs"))
  D:('$data(TCPQLEN)) trace^runtime("mw3:no TCPQLEN specified, using:"_^MW3S001TOBY("config","tcpQLen"))
  S ^MW3S001TOBY(svrInstId,"config","tcpPort")=$get(TCPPORT,^MW3S001TOBY("config","tcpPort"))
  S ^MW3S001TOBY(svrInstId,"config","maxJobs")=$get(MAXPROC,^MW3S001TOBY("config","maxJobs"))
  S ^MW3S001TOBY(svrInstId,"config","tcpQLen")=$get(MAXPROC,^MW3S001TOBY("config","tcpQLen"))
  ;
  S %WOS=$$engine^runtime()
  ;
  if (%WOS'="GT.M") W "only GT.M and YottaDB supported. sorry" Q
  ;
  S ^MW3S001TOBY(svrInstId,"run","mw3","st")="starting..."
  S ^MW3S001TOBY(svrInstId,"run","pid")=$job
  ;
  if $data(^DD) ; This just opens the main mumps.dat file so it can appear in lsof
  set SOC="SOC" ; Device ID
  D trace^runtime("D>mw3 SOC:"_SOC)
  D OpenSOC^sockMgr(SOC)
  ;
  ; -------- Open TCPCONN in main process --------
  ; here OPEN given TCP port and set it up
  D OpenListenTcpSOC^sockMgr(SOC,"mw3",^MW3S001TOBY(svrInstId,"config","tcpPort"),^MW3S001TOBY(svrInstId,"config","tcpQLen"))
  ; K. Now we are really really listening.
  set ^MW3S001TOBY(svrInstId,"run","mw3","st")="running"
  ; define character set as M default
  ; this is critical, otherwise server wont run
  ;todo: update to UTF-8
  use SOC:(CHSET="M")
  ;D trace^runtime("D>use SOC:(CHSET='M'")
  ;set listen with TCP queue lenght=5
  ;W /LISTEN(5) ; Listen 5 deep - sets $KEY to "LISTENING|socket_handle|portnumber"
  ;N SVRSOCK set SVRSOCK=$piece($KEY,"|",2)  ; LISTENING socket
  D trace^runtime("D>mw3 listen socket:"_$KEY)
  N %jobStop S %jobStop=0 ;false
  ;<debugging instrumentation
  ;N %debugcycles S %debugcycles=20
  ;N %dStartT,%dEndT S %dStartT=$horolog,%dEndT=%dStartT
  ;debugging instrumentation>
  ;---------- www server process main loop -----------
  N %jid,%sckH ;job id and client opened socket handle
  for  Q:((^MW3S001TOBY(svrInstId,"run","mw3","st")="stop"))  D
  . D trace^runtime("D>mw3 svr loop, st:"_^MW3S001TOBY(svrInstId,"run","mw3","st"))
  . ; <debug instrumentation
  . ;S %debugcycles=%debugcycles-1 S:('%debugcycles) ^MW3S001TOBY(svrInstId,"run","mw3","st")="stop"
  . ;if ($$DateDiff^datetime(%dStartT,%dEndT,"S")>30) do ;stop after xx sec.
  . ;. S ^MW3S001TOBY(svrInstId,"run","mw3","st")="stop"
  . ;S %dEndT=$horolog
  . ; debug instrumentation>
  . ; Wait until we have a connection (inifinte wait).
  . ; Stop if the listener asked us to stop.
  . ; W /WAIT 10sec. for $KEY for incomming socket comm
  . hang 0.1 ;slow it down in project development stage
  . ; check if not asked to stop
  . ;for  W /WAIT(10) Q:$KEY]"" ;code stops here until client socket open
  . W /WAIT(0.1) ;some relax time
  . ; todo: now various situations might appear
  . ; both tcp and local can appear here
  . ;D trace^runtime("D>mw3 KEY:"_$KEY)
  . ;set %sckH=$piece($KEY,"|",2)
  . ;when client connected make job
  . do:($piece($KEY,"|")="CONNECT") nextJob(svrInstId,$piece($KEY,"|",2))
  . D mw3jobMaint(svrInstId)
  ; end of loop
  close SOC set ^MW3S001TOBY(svrInstId,"run","mw3","st")="stopped"
  Q



;
Stop(svrInstId) ; tell the listener to stop running
  D trace^runtime("mw3.Stop("_svrInstId_")")
  set ^MW3S001TOBY(svrInstId,"run","mw3","st")="stop"
  Q



; private fn to get Job for incomming req
; w3job is separate process
; job receives id and socket from MW3
; communication w/ job is over ^MW3S001TOBY(svrInstId,"run","jobs",jobId)
; if nextJob returns -1, MW3 must HTTP503 refused
; nextJob looks only for jobs in state 3
nextJob:(svrInstId,%sckHndl)
  D trace^runtime("mw3.nextJob("_%sckHndl_")")
  N %jid,%ex,%st,%maxj,%rnJ ;jobId,existJobId,jobStat,maxJobs,numJobs
  S %st=0,%ex=0,%maxj=^MW3S001TOBY(svrInstId,"config","maxJobs"),%rnJ=$$countJobs(svrInstId)
  ;S jSock=$piece($KEY,"|",2)
  S %jid=^MW3S001TOBY(svrInstId,"run","jobs")+1 ;next job id
  ; if any job runing, try find existing free job
  ; before spawn new (save mem), retry=0 - quick search
  if (%jid>1) set %ex=$$getFreeJobId(svrInstId,0) set:(%ex>0) %jid=%ex,%st=3
  ; maxjobs already run, try getFreeJob again
  ; this time with retry (may wait up to 300ms)
  ;todo:id grows as jobs crash, can be 1,3,6,7,10,...
  else  if (%rnJ>%maxj) set %ex=$$getFreeJobId(svrInstId,3) set:(%ex>0) %jid=%ex,%st=3
  ; if found free job, reuse it
  ;set:(%ex>0) %jid=%ex,%st=3
  ; reached maxJobs & none is free
  D trace^runtime("D>mw3 jobs state %jid:"_%jid_",%maxj:"_%maxj_",%ex:"_%ex_",%rnJ:"_%rnJ_",%st:"_%st)
  if ((%jid>%maxj)&(%ex<1)) do  Q
  . ;server busy, must refuse
  . D trace^runtime("D>mw3: server busy, do HTTP503 refused")
  . ;todo: check if correct SOC spec !!!
  . D SendErr^httRes(SOC,503,"connection refused")
  . ;use SOC:(detach=%sckHndl) ;detach from MW3
  . ;close socket
  . D CloseSOCS^sockMgr(SOC,%sckHndl)
  . ;C SOC:(socket=%sckHndl)
  . ;else  D trace^runtime("D>mw3: err, socket "_%sckHndl_" close failed !!")
  ; ready to reuse or spawn new :)
  ;todo:lock??
  S:(%st=0) ^MW3S001TOBY(svrInstId,"run","jobs")=%jid ;jobs++ for new job only
  ;can set handle, job status is still unchanged
  S ^MW3S001TOBY(svrInstId,"run","jobs",%jid,"sock")=%sckHndl
  ;S ^MW3S001TOBY(svrInstId,"run","jobs",%jid,"sock")=""
  ; now detach client socket from MW3
  use SOC:(detach=%sckHndl) ;detach from MW3
  D trace^runtime("D>mw3 socket w/ handle:"_%sckHndl_" detached")
  ;
  if (%st=3) D
  . ;update status to get ready for new tcp sock
  . D trace^runtime("D>mw3 set jobs "_%jid_" to st:7")
  . ;set job to get ready for new tcp sock
  . S ^MW3S001TOBY(svrInstId,"run","jobs",%jid,"st")=7,^MW3S001TOBY(svrInstId,"run","jobs",%jid,"sock")=%sckHndl
  ; or spawn new job
  else  if (%st=0) do startJob(svrInstId,%jid,%sckHndl)
  ;todo: error handling (outOfResource,...)
  ;todo:unlock
  Q



mw3jobMaint:(svrInstId)
  D trace^runtime("D>mw3.mw3jobMaint()")

  for %j=-1:0 S %j=$order(^MW3S001TOBY(svrInstId,"run","jobs",%j)) Q:(%j="")  D
  . if ($get(^MW3S001TOBY(svrInstId,"run","jobs",%j,"st"),0)=15) D passTcpSocket(%j,^MW3S001TOBY(svrInstId,"run","jobs",%j,"sock"))
  ;
  D trace^runtime("D>mw3.mw3jobMaint done")
  Q



;
passTcpSocket:(jobId,sockAtt)
  D trace^runtime("D>mw3.passTcpSocket()")
  N LOC S LOC="LOC"
  D OpenSOC^sockMgr(LOC)
  D OpenConnectLSOC^sockMgr(LOC,"commSockId"_jobId,"commSocAttchId"_jobId)
  U LOC:(socket="commSocAttchId"_jobId)
  D trace^runtime("D>mw3:W /PASS")
  W /PASS(,,sockAtt)
  else  D trace^runtime("D>mw3:err pass tcp socket failed")
  D trace^runtime("D>mw3:pass done")
  ;revert back to server socket
  U SOC:(socket="mw3")
  ;C LOC
  D CloseSOCS^sockMgr(LOC,"commSocAttchId"_jobId)
  D CloseSOC^sockMgr(LOC)
  Q



;private fn lookup existing jobs for free one
;todo:remove hang ??
getFreeJobId:(svrInstId,tstRtry)
  ;%r number of retries
  ;%st - job status (-1=err,0=new,1=running,2=wait4job)
  N %now S %now=$horolog
  D trace^runtime("D>mw3.getFreeJobId("_tstRtry_")")
  N %j,%st,%r,%n S %st=0,%r=tstRtry,%n=0
  for  Q:((%r<0)!(%st=3))  do  ;retry loop
  . for %j=-1:0 S %j=$O(^MW3S001TOBY(svrInstId,"run","jobs",%j)) Q:((%j="")!(%st=3))  do  ;search loop
  . . S %st=^MW3S001TOBY(svrInstId,"run","jobs",%j,"st"),%n=%j
  . . D trace^runtime("D>mw3 getFreeJobId,id:"_%j_",st:"_%st)
  . S %r=%r-1 hang 0.1
  S:(%st'=3) %n=0 ;no free job available
  D trace^runtime("D>mw3 use free job:"_%n_",st:"_%st)
  Q %n



;todo:clean job when it dies (stops update jobUpdTime)
;not used now
countJobs:(svrInstId)
  D trace^runtime("D>mw3.countJobs")
  N %now,%j,%n S %now=$horolog,%n=0
  for %j=-1:0 S %j=$O(^MW3S001TOBY(svrInstId,"run","jobs",%j)) Q:(%j="")  do
  . D trace^runtime("D>mw3:debug "_$$DateDiff^datetime($get(^MW3S001TOBY(svrInstId,"run","jobs",%j,"jobUpdTime"),"0,0"),%now,"S"))
  . if ($$DateDiff^datetime($get(^MW3S001TOBY(svrInstId,"run","jobs",%j,"jobUpdTime"),%now),%now,"S")>10) do
  . . D trace^runtime("D>mw3:kill job:"_%j)
  . . K ^MW3S001TOBY(svrInstId,"run","jobs",%j)
  . else  S %n=%n+1
  D trace^runtime("mw3 countJobs:"_%n)
  Q %n



;
startJob:(svrInstId,JobId,tcpSockHndl)
  D trace^runtime("mw3.StartJob()")
  S MW3S001TOBY("run","jobs",%j,"sock")=tcpSockHndl
  N QT,ARG set QT="""",ARG=QT_"SOCKET:"_tcpSockHndl_QT
  ;todo: test locate w3job routine!!! it is not on path
  ;release version will load from .so so no need for physical path
  ;N cmd set cmd="StartJob^w3job($get(%jid)):(input="_ARG_":output="_ARG_")"
  ;debug version #1
  ;N cmd set cmd="rj^runtime("_QT_"server/mw3/w3job"_QT_","_QT_"StartJob^w3job"_QT_","_QT_$principal_QT_","_JobId_"):(input="_ARG_":output="_ARG_")"
  N cmd set cmd="rj^runtime("_QT_"server/mw3/w3job"_QT_","_QT_"StartJob^w3job"_QT_","_QT_svrInstId_QT_","_JobId_"):(input="_ARG_":output="_ARG_":error="_ARG_")"
  D trace^runtime("D>mw3 jobCmd:"_cmd)
  job @cmd
  ; debug version #2 (not working now)
  ;job r^runtime("server/mw3/w3job","StartJob^w3job",$get(%jid)):(input="@ARG":output="@ARG")
  D trace^runtime("D>mw3.StartJob done")
  Q



;
resetJobs:(svrInstId)
  ;reset jobs
  D trace^runtime("D>mw3.resetJobs()")
  ;Q:('$data(^MW3S001TOBY(svrInstId,"run","jobs")))
  K ^MW3S001TOBY(svrInstId,"run","jobs")
  S ^MW3S001TOBY(svrInstId,"run","jobs")=0
  D trace^runtime("D>mw3 resetJobs done"_^MW3S001TOBY(svrInstId,"run","jobs"))
  Q



; init server configuration
defaults:()
  D init()
  K MW3S001TOBY ;N MW3S001TOBY

  ;set how many parallel jobs can MW3 run
  if ('$data(^MW3S001TOBY("config"))) do
  . S ^MW3S001TOBY("config","ver")="0.0.1"
  . S ^MW3S001TOBY("config","maxJobs")=5
  . S ^MW3S001TOBY("config","tcpQLen")=5
  . S ^MW3S001TOBY("config","tcpPort")=8080
  ;set trace msg output
  ;S ^MW3S001TOBY("config","traceOut")=$principal
  ;copy config from global into mem var
  merge MW3S001TOBY("config")=^MW3S001TOBY("config")
  ;reset trace
  ;do setTrace^runtime(1,$principal)
  ;D trace^runtime("D>mw3 defaults, trace ON")
  Q



;
;errReport
;  ;s $ETRAP="Q"
;  D trace^runtime("D>err")
;  D trace^runtime($ZSTATUS)
;  D trace^runtime($ZERROR)
;  Q
