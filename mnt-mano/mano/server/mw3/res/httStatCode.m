init() ;awport.pro 2022-01-26, please read LICENSE before use
  S HTTPSTAT(200)="OK"
  S HTTPSTAT(201)="Created"
  S HTTPSTAT(400)="Bad Request"
  S HTTPSTAT(401)="Unauthorized"
  S HTTPSTAT(403)="Not Found"
  S HTTPSTAT(404)="Method Not Allowed"
  S HTTPSTAT(408)="Request Timeout"
  S HTTPSTAT(413)="Payload Too Large"
  S HTTPSTAT(414)="URI Too Long"
  S HTTPSTAT(415)="Unsupported Media Type"
  S HTTPSTAT(500)="Internal Server Error"
  Q



;httMsg is optional
GetResStatLine(httStat,httpMsg)
  S:('$data(httpMsg)) httpMsg=""
  D trace^runtime("D>httStatCode.GetResStatLine("_httStat_","_httpMsg_")")
  N %proto S %proto="HTTP/1.1"
  ;D trace^runtime("D>stat:"_$G(HTTPSTAT(httStat)))
  Q:$data(HTTPSTAT(httStat)) %proto_" "_httStat_" "_HTTPSTAT(httStat)
  ;N %c S %c=$piece(httStat,1,1)
  ;do trace^runtime(%proto_" 400 "_HTTPSTAT(400))
  Q %proto_" 400 "_HTTPSTAT(400)
