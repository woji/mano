init() ;awport.pro 2022-01-27, please read LICENSE before use

  Q
;todo: if written output is larger than 1MB open file on ramdisk and write all into


;
defaults:()
  S mlangBuffWriter=0,mlangBuffWriter("config","bufferSize")=32000
  Q



;
Open(devId,bufferSZ,type)
  Q:($$exists(devId)!'$L(devId))
  D trace^runtime("D>bufferWriter.Open")
  ;todo:validate buff size, < 1MB
  S mlangBuffWriter(devId,"buffSZ")=$get(bufferSZ,mlangBuffWriter("config","bufferSize"))
  S mlangBuffWriter(devId,"type")=$$type(type)
  S mlangBuffWriter(devId,"inuse")=0
  S mlangBuffWriter(devId,"data")=""
  Q



;
Close(devId)
  Q:'($$exists(devId)!'$L(devId))
  D trace^runtime("D>bufferWriter.Close()")
  D:(mlangBuffWriter(devId,"inuse")) Flush(devId)
  K mlangBuffWriter(devId)
  Q



;
WriteBuff(devId,data)
  Q:('$$exists(devId))
  D trace^runtime("D>bufferWriter.WriteBuff()")
  D trace^runtime("D>bufferWriter:write data:"_data)
  if ($ZL(mlangBuffWriter(devId,"data"))+$ZL(data))>mlangBuffWriter(devId,"buffSZ") D Flush(devId)
  ;I $L($SY,":")=2,$L(%WBUFF)+$L(DATA)>32000 D FLUSH
  S mlangBuffWriter(devId,"data")=mlangBuffWriter(devId,"data")_data
  ;D trace^runtime("D>bufferWriter:data:"_data)
  S mlangBuffWriter(devId,"inuse")=1
  Q



;writes data into current buffer
;WriteCurrent(data)
;  ;todo:check if any dev opened !!!
;  N devId S devId=$order(mlangBuffWriter(""))
;  D trace^runtime("D>bufferWriter.WriteCurrent()")
;  D WriteBuff(.devId,.data)
;  Q



;
Flush(devId) ;
  Q:('($$exists(devId)&$$canFlush(devId)))
  ; ZEXCEPT: %WBUFF - Buffer in Symbol Table
  D trace^runtime("D>bufferWriter.flush()")
  ;D trace^runtime("D>w3res:flush "_%WBUFF)
  W mlangBuffWriter(devId,"data"),!
  U:($$canFlush(devId)) devId:flush
  D trace^runtime("D>bufferWriter device flush command ok")
  S mlangBuffWriter(devId,"data")=""
  S mlangBuffWriter(devId,"inuse")=0
  D trace^runtime("D>bufferWriter.flush() ok")
  Q



;returns type of data storage (socket,string,file,sglv)
ContentType(devId)
  Q:(mlangBuffWriter(devId,"type")="SOC") "socket"
  Q:(mlangBuffWriter(devId,"type")="SD") "file:"_mlangBuffWriter(devId,"file")
  Q:(mlangBuffWriter(devId,"type")="MEM"&'$data(mlangBuffWriter(devId,"file"))) "string"
  Q:(mlangBuffWriter(devId,"type")="MEM") "file:"_mlangBuffWriter(devId,"file")
  ;todo slgv
  Q ""



;returns underlying data when possible
ToString(devId)
  Q mlangBuffWriter(devId,"data")



;
exists:(devId)
  ;all public methods uses exists, call defaults here
  do:('$data(mlangBuffWriter)) defaults
  Q $data(mlangBuffWriter(devId))



;validate device type
type:(inType)
  ;todo:
  Q inType



;
canFlush:(devId)
  N retV S retV=0
  S:(mlangBuffWriter(devId,"type")="SOC") retV=1 ;socket device
  S:(mlangBuffWriter(devId,"type")="SD") retV=1 ;sequential disk devce
  Q retV
