init() ; awport.pro, 2022-01-16, please read LICENSE before use

  Q



;
GMT() ; return HTTP date string (this is really using UTC instead of GMT)
  ;do trace^runtime("GMT()")
  N retV
  N %dtp S %dtp="datetimepipe"
  N OLDIO S OLDIO=$IO
  O %dtp:(shell="/bin/sh":comm="date -u +'%a, %d %b %Y %H:%M:%S %Z'|sed 's/UTC/GMT/g'")::"pipe"
  U %dtp R retV:1
  U OLDIO C %dtp
  ;do trace^runtime("GMT:"_retV)
  Q retV
