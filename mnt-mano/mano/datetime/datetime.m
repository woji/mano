init() ; awport.pro, 2022-01-16, please read LICENSE before use
  do import^runtime("datetime/datetimeDiff")
  do import^runtime("datetime/datetimeGMT")
  Q



;
DateDiff(hDate1,hDate2,strInterval) Q $$DateDiff^datetimeDiff(.hDate1,.hDate2,.strInterval)

GMT() Q $$GMT^datetimeGMT()
