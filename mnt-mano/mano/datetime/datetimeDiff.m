init() ; awport.pro, 2022-01-16, please read LICENSE before use

  Q



;
DateDiff(hDate1,hDate2,strInterval)
  if (strInterval'="S") Q ""
  N sec1,sec2 S sec1=$piece(hDate1,",",2),sec2=$piece(hDate2,",",2)
  if (sec1>sec2) S sec1=sec1-86400
  Q sec2-sec1
