init() ;awport.pro 2022-01-15, please read LICENSE before use

  Q



;
ltrim(str) ; Trim whitespace from left side of string
 ; derived from XLFSTR, but also removes tabs
 N %L,%R
 S %L=1,%R=$L(str)
 F %L=1:1:$L(str) Q:$A($E(str,%L))>32
 Q $E(str,%L,%R)
