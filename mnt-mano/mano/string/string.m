init() ; awport.pro, 2022-01-16, please read LICENSE before use

  ;import^runtime("json/jsonParse")
  do import^runtime("string/stringReplace")
  do import^runtime("string/stringTrim")
  ;zlink "./jsonParse.m"
  ;zlink "./jsonStringify.m"
  Q


; convert json string into closed array
;parse(VVJSON) goto %parse^jsonParse
replace(str,from,to) Q $$replace^stringReplace(.str,.from,.to)
;stringify(data) q "test"

ltrim(str) Q $$ltrim^stringTrim(.str)

toLower(str) Q $zconvert(str,"L")
toUpper(str) Q $zconvert(str,"U")

test do test^stringReplace Q
