init() ;awport.pro 2022-01-15, please read LICENSE before use

  Q



;
replace(str,from,to)
  N retVal S retVal=""
  F i=1:1:$L(str,from) D
  . S:i=1 retVal=retVal_$P(str,from,i)
  . S:i>1 retVal=retVal_to_$P(str,from,i)
  Q retVal



; "from" must not be contained in "to" and vice versa
; but overal nice code :)
replaceUnsafe(str,from,to)
  F i=1:1 Q:'$F(str,from)  D
  . S str=$P(str,from)_to_$P(str,from,2,$L(str,from))
  Q str



;
test()
  W $$replace("jaja a adam a madamadam","adam","ada")
  ;result in "jaja a ada a madaada"
  Q
