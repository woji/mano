init() ;awport.pro, 2022-02-01, please read LICENSE before use
  do import^runtime("server/mw3/httReq")
  do import^runtime("server/mw3/httRes")
  do import^runtime("stream/bufferWriter")
  Q


Routine1(ioId)
  ;in runtime W3JOB() is available here
  ;bufferWriter is open now and waits for body data
  N title,head,msg,retV
  S title="Routine1",head="Routine1",msg="Welcome, lucky you<br/>buďte vítáni s přáním štěstí<br/>ようこそ、幸運なあなた"
  S retV="<html><head><title>"_title_"</title></head><body><h1>"_head_"</h1><div style='display:flex;flex-direction:row'><div style='font-size:50pt;width:50pt'>:)</div><div><p>"_msg_"</p></div></div></body></html>"
  D WriteBuff^bufferWriter(ioId,retV)
  Q
