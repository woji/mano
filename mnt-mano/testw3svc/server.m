init() ;awport.pro, 2022-02-01, please read LICENSE before use
  D import^runtime("server/mw3")
  D import^runtime("/testw3svc/testw3svcRoutes")
  ;just for test remove it
  ;do import^runtime("server/mw3/urlRouter")
  Q



;to run server do:
; docker exec -it[containerId] /bin/bash
; source /opt/yottadb/current/ydb_env_set && export ydb_routines="/mano $ydb_routines" && /opt/yottadb/current/yottadb -dir
;D setTrace^runtime("D",$principal)
;D import^runtime("/testw3svc/server")
;D Run^server
;
;or one row...
;D setTrace^runtime("I",$principal) D import^runtime("/testw3svc/server") D Run^server
;
;test:
;curl -X GET http://awport.pro:9080
;curl -X GET http://192.168.56.8:9080
;
; regular stop
;D Stop^server
;
;emergency stop
;W "404" C SOC U 0 S ^MW3S001TOBY("run","mw3","st")="stop"
Run()
  ; example web service application
  ;D setTrace^runtime("I",$principal)
  D trace^runtime("testw3svc.Run()")
  N svrName S svrName="testw3svc"
  D Delete^mw3(svrName)
  D Add^mw3(svrName)
  D LoadRoutes^testw3svcRoutes(svrName)
  ;test routes
  ;D trace^runtime($$FindRoute^urlRouter(svrName,"/","GET"))
  job rj^runtime("server/mw3","Listen^mw3",svrName,8080,5,5)

  Q



;stop test server
Stop()
  ;
  N svrName S svrName="testw3svc"
  D Stop^mw3(svrName)
  ;D Delete^mw3(svrName)
  Q
