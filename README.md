# MANO
<img src="https://gitlab.com/woji/mano/-/raw/main/mnt-mano/mano/mano-logo.png" width="200" alt="logo">

is student project made for anybody who is interested in MUMPS and YOTTADB (ydb). Its primary purpose is to learn how to build www server to fetch data stright out of ydb without any INETD, nodem, QEWD, etc by directly use ydb great support for network communication (as it is primarily created for critical banking and hospital apps). In this way one can easily interact ydb with web browsers by exchange textual/json data between server and browser.

project is called framework because ydb (MUMPS) function's set is very tiny and complex software such as full http server requires a many various functions. The [MWS](https://github.com/shabiel/M-Web-Server) was something I originaly planned to use. But it is mashup of many previous projects written in many (sometimes obscure) styles and when somebody starts with M-lang, will get probably frustrated how unreadable and dirty MWS code is. This led me to question how to organise source filestructure to make it maintanable. As nodejs fan I decided to copy as much as posible of its concept. So idea of framework/modules set was born :)

## status
current implementation supports only HTTP 1.1 GET method and can fetch 1MB output as string that can be written to output by custom m-routine.

Server now handles incoming tcp requests through jobs, so there is single main process (mw3.m) that passes incoming socket to dedicated job (w3job.m) and each job runs as separate process.
Jobs are then reused for future requests.

*removed debug version limit to 40sec of runtime of both main and job, now runs permanently*

### where to start
for more info about yottadb, please see [this](https://docs.yottadb.com/ProgrammersGuide/index.html)

to work with m-language easily please use [this](https://gitlab.com/asida/atom-language-m-lang) plugin for [atom](https://atom.io) editor and [remote-editor](https://atom.io/packages/remote-editor)

please read **runtime.m** first, this is framework loader that uses great yottadb compiler/linker advantage and can load routine into memory from anywhere.
It helped me to organise source files into folders and avoid write compressed and "all in one" folder code, more about it later.

>mano web server code, called mw3 (mumps www) server is currently located under /mano/server/mw3 folder

## Installation
ydb itself don't have yarn, deb or whatever package,... so probably best way to not mess your machine with non reversible 1GB of C-build ballast is use source tree attached dockerfile and docker-compose.yml.

So whole setup is manual process step by step:

1. make container image
2. adopt docker-compose
3. run container and exec into its shell to play with mano

### dockerfile
dockerfile is taken from ydb source tree and adopted to VmWare minideb, because Ubuntu...is just fat.

>dockerfile is so far from being good and production usable, c-build process produces 20MB tiny ydb and.. 450MB image !!!!

dockerfile contains some extras:

- mkdir /mano to mount host ./mnt-mano
- mkdir /tmp/streams this is RAM disk tmp filesystem for www server (not used now)
- use FROM bitnami minideb image instead of massive Ubuntu/Debian

>to get some help how to build container see docker-build.sh

>*note docker-build.sh is not runnable, go line by line by your needs*

traditional MUMPS systems expects to have data & programs in same folder "nested" like:

```
/data/r1.32_x86_64/o/utf8*(/data/r1.32_x86_64/r /data/r)

#which means
#compiled routines goes here
/data/r1.32_x86_64/o/utf8*
#source m code goes here
/data/r1.32_x86_64/r
#and alternatively here
/data/r
#data (globals) goes here
/data/r1.32_x86_64/g
```
this scheme is uncomfortable for containerisation and for software package distribution and manintenance.
MANO package comes with idea of separate its code from /data into /mano and leaves original yottadb filestructure intact.
And thats why docker image makes mkdir /mano

### docker-compose
next step is docker-compose.yml which expects 2 folders:

- mnt-data - this is original filestructure for data and "other than mano" routines for ydb
- mnt-mano - special folder for MANO files explained above (both routines and compiled objects)

>by pulling this project, one would miss mnt-data folder that is not distributed with code, because it simply contains my own data :) which is just ballast for others. Therefore first what one needs to do after code pull is to create data folder mnt-data within root of code structure or update docker-compose.yml with own name and location of

## Usage
first lets test to run ydb like:

```
#consider sudo su
docker-compose up -d
#check if running and get containerID
docker ps
# get into container shell
docker exec -it [containerID] /bin/bash
#setup environment
source /opt/yottadb/current/ydb_env_set
# add path to mano routines
export ydb_routines="/mano $ydb_routines"
#finally run YDB> console
/opt/yottadb/current/yottadb -dir
```

if all is ok one should see:

```
YDB>
```
so try
```
YDB>D setTrace("D",$principal)
YDB>D trace^runtime("BOO!!")
2022-02-09T10:00:00 BOO!!
```

this is correct output. install and setup seems ok.

### testw3svc - MW3 test project
soon after I wrote first version of MW3 I found that it would be very convenient to use it as nodejs express.js
so I updated source a bit and put all framework code under /mano folder (lets pretend it is node-modules folder) and created testw3svc folder(project) that utilizes /mano framework(modules)
so feel free to create your own folder aside of testw3svc and create own service instance

>dont forget to change service tcp port for each project!!

best way to run test server is:

```
YDB>D setTrace^runtime("I",$principal) D import^runtime("/testw3svc/server") D Run^server
```

these 3 commands will:

- setup trace to informative
- compile,link&load routine file testw3svc/server and all its dependencies
- run sample instance of www server with values defined in server.m file

>server runs and one can
curl -X GET http://192.168.XX.XX:9080
to see test output

## debuging and tracing
as mano www server is non interactive code I wrote m-lang functions setTrace^runtime and trace^runtime who can set 3 levels of trace output

- no tracing info, just type into ydb console:

```
YDB>D setTrace("")
```
- informative trace output, type:

```
YDB>D setTrace("I",$principal)
```
- detailed debug trace output, type:

```
YDB>D setTrace("D",$principal)
```

>*note that $principal saves current terminal device as trace output device and all calls of trace^runtime is then directed to this output*

## Roadmap
implement:

- POST, OPTIONS http methods
- json body parser to make mano use comfortable

## Authors and acknowledgment
this software is created by Adam Wojkowski wojkowski.free@gmail.com

## License
see [license](./license.md)
