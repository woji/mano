# this is step by step guide how to build container
# not automatic script !!
# dont use https://hub.docker.com/ coz this image is bloated !!!
# yottadb team need to hard work on it before publish
# now just build locally

# delete old image (when necessary)
docker image rm "yottadb/mano:v6"

cd ./docker-build
docker build --tag "yottadb/mano:v6" .

#transfer image to another server
#save
docker save -o ./yottadb-mano.tar yottadb/mano:v6
#todo:transfer ssh...
#and load
docker load -i ./yottadb-mano.tar

#now can start by docker-compose
