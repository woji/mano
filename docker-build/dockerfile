#################################################################
#								#
# Copyright (c) 2018-2021 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#
# adopted by adam.sida 2022-01-13
# for MANO
# removed go and make from end of orig. script
# todo: replace wget with curl
#								#
#################################################################
# See README.md for more information about this Dockerfile
# Simple build/running directions are below:
#
# Build:
#   $ docker build -t yottadb/yottadb:latest .
#
# Use with data persistence:
#   $ docker run --rm -v `pwd`/ydb-data:/data -ti yottadb/yottadb:latest

ARG OS_VSN=bullseye
#ARG DEBIAN_FRONTEND=noninteractive
# Stage 1: YottaDB build image
#FROM debian:${OS_VSN} as ydb-release-builder
FROM bitnami/minideb:${OS_VSN} as ydb-release-builder

ARG CMAKE_BUILD_TYPE=Release
#added wget, ca-certificates (gitlab tls)
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
                    file \
                    cmake \
                    make \
                    gcc \
                    pkg-config \
                    git \
                    ca-certificates \
                    wget \
                    tcsh \
                    libconfig-dev \
                    libelf-dev \
                    libgcrypt-dev \
                    libgpg-error-dev \
                    libgpgme11-dev \
                    libicu-dev \
                    libncurses-dev \
                    libssl-dev \
                    zlib1g-dev \
                    && \
    apt-get clean

#stage 2: download YottaDB source
ARG ydb_distrib=https://gitlab.com/api/v4/projects/7957109/repository/tags
ARG ydb_srcdir=/tmp/yottadb-src
ARG ydb_tmpdir=/tmp/yottadb-tmp
RUN mkdir $ydb_srcdir && \
  mkdir $ydb_tmpdir && \
  wget -P $ydb_tmpdir ${ydb_distrib} 2>&1 1>${ydb_tmpdir}/wget_latest.log
#quite tricky how to fill variable with shell output...
#see <https://stackoverflow.com/questions/34911622/dockerfile-set-env-to-result-of-command>
RUN sed 's/,/\n/g' ${ydb_tmpdir}/tags | grep -E "tag_name|.pro.tgz" | grep -B 1 ".pro.tgz" | grep "tag_name" | sort -r | head -1 | cut -d'"' -f6 > ./ydb_version
RUN git clone --depth 1 --branch $(cat ./ydb_version) https://gitlab.com/YottaDB/DB/YDB.git $ydb_srcdir
#cd YDB


ENV GIT_DIR=/tmp/yottadb-src/.git
RUN mkdir -p /tmp/yottadb-build \
 && cd /tmp/yottadb-build \
 && test -f /tmp/yottadb-src/.yottadb.vsn || \
    grep YDB_ZYRELEASE /tmp/yottadb-src/sr_*/release_name.h \
    | grep -o '\(r[0-9.]*\)' \
    | sort -u \
    > /tmp/yottadb-src/.yottadb.vsn \
 && cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=/tmp \
      -D YDB_INSTALL_DIR:STRING=yottadb-release \
      -D CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} \
      /tmp/yottadb-src \
 && make -j $(nproc) \
 && make install

# Stage 3: YottaDB release image
ARG OS_VSN=bullseye
#ARG DEBIAN_FRONTEND=noninteractive

#FROM debian:${OS_VSN} as ydb-release
FROM bitnami/minideb:${OS_VSN} as ydb-release

#todo: check for gzip,date,sed,curl,perl because of MWS install
#2022-01-18 added nano
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
                    file \
                    binutils \
                    ca-certificates \
                    libelf-dev \
                    libicu-dev \
                    locales \
                    wget \
                    #curl \
                    #vim \
                    procps \
                    pkg-config \
                    nano \
                    && \
    apt-get clean
#todo:check for consequences with cs_CZ
#
# added mk ramdisk folder for MANO
#
RUN locale-gen en_US.UTF-8 && \
  mkdir /tmp/streams && chmod 666 /tmp/streams && \
  mkdir /mano && chmod 777 /mano

COPY --from=ydb-release-builder /tmp/yottadb-release /tmp/yottadb-release
RUN cd /tmp/yottadb-release  \
 && pkg-config --modversion icu-io \
      > /tmp/yottadb-release/.icu.vsn \
 && ./ydbinstall \
      --utf8 `cat /tmp/yottadb-release/.icu.vsn` \
      --installdir /opt/yottadb/current \
 && rm -rf /tmp/yottadb-release
ENV ydb_dir=/data \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=C.UTF-8 \
    ydb_chset=UTF-8

WORKDIR /data

# MUPIP RUNDOWN need in the following chain because otherwise ENTRYPOINT complains
# about inability to run %XCMD and rundown needed. Cause not known, but this workaround works
# and is otherwise benign.
RUN . /opt/yottadb/current/ydb_env_set \
 && echo "zsystem \"mupip rundown -relinkctl\"" | /opt/yottadb/current/ydb -dir
ENTRYPOINT ["/opt/yottadb/current/ydb"]
CMD ["-run", "%XCMD", "zsystem \"bash\""]
